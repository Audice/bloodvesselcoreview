﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CoreViewVisionTester.VideoHandler;
using CoreViewVisionTester.VideoHandler.CatheterModel;
using CoreViewVisionTester.VideoHandler.EventsArgs;
using Emgu.CV;
using Emgu.CV.Structure;

namespace CoreViewVisionTester
{
    public partial class Form1 : Form
    {

        FrameCapture cap;
        FrameHandler frameHandler;
        CatheterWire catheter;
        public Form1()
        {
            InitializeComponent();
            cap = new FrameCapture(0);
            cap.ExceptionRiseEvent += ExceptionMessageShow;
            cap.FrameReadyEvent += viewframe;
            
            frameHandler = new FrameHandler(480, 640, cap);
            frameHandler.FrameReadyEvent += PictureBoxFill;
            this.ThresholdLabel.Text = "Порог: " + this.frameHandler.ThresholdValue.ToString();
            this.ThresholdTrack.Value = this.frameHandler.ThresholdValue;
            catheter = new CatheterWire(frameHandler); //(catheterSkelet);
            catheter.CatheterLineChangeEvent += PictureBoxFill;
            
        }

        private void viewframe(object sender, FrameReadyEventArgs e)
        {
            this.pictureBox4.Image = e.Gray_Frame.ToBitmap();
        }

        private void ExceptionMessageShow(object sender, ExceptionEventArgs e)
        {
            MessageBox.Show("Сообщение: " + e.ExceptionMessage + Environment.NewLine + "Подробности: " + e.ExceptionStackTrace, "Ошибка камеры");
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            cap.VideoCaptureStart();
        }

        void PictureBoxFill(object sender, CatheterLineEventArgs e)
        {
            CatheterPoint startCatheterPoint = e.StartPoint;
            CatheterPoint finishCatheterPoint = e.EndPoint;

            Bitmap bm = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics g = Graphics.FromImage(bm);
            g.Clear(Color.White);

            if (e.StartPoint != null)
            {
                g.DrawEllipse(new Pen(Color.Black, 3), startCatheterPoint.X, startCatheterPoint.Y, 5, 5);
            }

            if (e.CatheterPoints != null && e.CatheterPoints.Count > 0)
            {
                for (int i = 0; i < e.CatheterPoints.Count; i++)
                {
                    g.DrawEllipse(new Pen(Color.Red, 3), e.CatheterPoints[i].X, e.CatheterPoints[i].Y, 5, 5);
                }
                for (int i = 1; i < e.CatheterPoints.Count; i++)
                {
                    g.DrawLine(new Pen(Color.Red, 2), e.CatheterPoints[i-1].X, e.CatheterPoints[i-1].Y, e.CatheterPoints[i].X, e.CatheterPoints[i].Y);
                }
            }
            this.pictureBox1.Image = bm;
            if (e.AfterCatheterSearchingFrame != null)
                this.pictureBox3.Image = e.AfterCatheterSearchingFrame.ToBitmap();
        }


        void PictureBoxFill(object sender, FrameReadyEventArgs e)
        {
            this.label3.Invoke(new Action(() => this.label3.Text = "FPS: " + e.Framerate.ToString() ));
            if (e.Sobel_Frame != null)
                this.pictureBox2.Image = e.Sobel_Frame.ToBitmap();
        }

        private void ThresholdTrack_ValueChanged(object sender, EventArgs e)
        {
            if (this.frameHandler != null)
            {
                this.frameHandler.ThresholdValue = (byte)(this.ThresholdTrack.Value);
                this.ThresholdLabel.Text = this.ThresholdTrack.Value.ToString();
            }
        }

        private void pictureBox3_MouseClick(object sender, MouseEventArgs e)
        {
            PrintCoord(e);
        }

        void PrintCoord(MouseEventArgs e)
        {
            int x = e.X;
            int y = e.Y;
            ClickCoord.Text = "X: " + x.ToString() + "Y: " + y.ToString();
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            PrintCoord(e);
        }

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            PrintCoord(e);
        }

        private void pictureBox4_MouseClick(object sender, MouseEventArgs e)
        {
            PrintCoord(e);
        }
    }
}
