﻿using CoreViewVisionTester.VideoHandler.EventsArgs;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler
{
    public class FrameHandler
    {
        public byte ThresholdValue { get; set; } = 60;

        //Debug fields
        /// <summary>
        /// Поле таймера, для подсчёта среднего фреймрейта
        /// </summary>
        Stopwatch FrameCalc = null;


        private readonly ushort ExceptionBorderValue = 5;
        /// <summary>
        /// Предыдущий кадр видеопотока
        /// </summary>
        private Image<Gray, Byte> PreviousFrame;

        private Image<Gray, Byte> StartFrame;

        /// <summary>
        /// Ширина кадра
        /// </summary>
        public ushort FrameWidht { get; private set; }
        /// <summary>
        /// Высота кадра
        /// </summary>
        public ushort FrameHeight { get; private set; }

        //Обработчик события появления нового кадра
        public event EventHandler<FrameReadyEventArgs> FrameReadyEvent;
        protected virtual void OnFrameReady(FrameReadyEventArgs e)
        {
            EventHandler<FrameReadyEventArgs> handler = FrameReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public FrameHandler(int frameWidht, int frameHeight, FrameCapture frameCapture)
        {
            FrameCalc = new Stopwatch();
            PreviousFrame = null;
            StartFrame = null;
            if (frameCapture != null)
                frameCapture.FrameReadyEvent += FramePrepare;
        }

        private void InitStartFrame(Image<Gray, Byte> startFrame)
        {
            this.StartFrame = new Image<Gray, byte>(startFrame.Data);
        }

        void FramePrepare(object sender, FrameReadyEventArgs e)
        {
            if (StartFrame == null) InitStartFrame(e.Gray_Frame);
            if (PreviousFrame == null)
            {
                PreviousFrame = e.Gray_Frame;
                return;
            }
            Image<Gray, Byte> currentFrame = e.Gray_Frame;
            //Выделене катетера на кадре
            var assignmentResult = AssignmentCatheter(currentFrame, this.StartFrame);
            //Генерация события готовности подготовленных кадров
            FrameReadyEventArgs args = new FrameReadyEventArgs();
            //args.Gray_Frame = resultImage;
            args.Sobel_Frame = assignmentResult;
            args.Framerate = this.CalcFrameTime();

            this.OnFrameReady(args);
            //Очистка памяти
            PreviousFrame.Dispose();
            //Меняем предыдущий кадр на текущий
            PreviousFrame = currentFrame;
        }

        private ushort CalcFrameTime()
        {
            ushort res = 0;
            if (this.FrameCalc.IsRunning)
            {
                FrameCalc.Stop();
                long milis = FrameCalc.ElapsedMilliseconds;
                res =(ushort)(1000.0 / milis);
                FrameCalc.Reset();
                FrameCalc.Start();
            }
            else
            {
                FrameCalc.Start();
            }
            return res;
        }

        Image<Gray, float> SobelCalculate(Image<Gray, byte> nextFrame)
        {
            Image<Gray, float> sobely = nextFrame.Sobel(0, 1, 3);
            Image<Gray, float> sobelx = nextFrame.Sobel(1, 0, 3);
            Emgu.CV.CvInvoke.Multiply(sobelx, sobelx, sobelx);
            Emgu.CV.CvInvoke.Multiply(sobely, sobely, sobely);
            Emgu.CV.CvInvoke.Sqrt(sobelx + sobely, sobely);

            return sobely;
        }
        Image<Gray, Byte> AssignmentCatheter(Image<Gray, Byte> newFrame, Image<Gray, Byte> baseFrame)
        {
            //Откасзываемся от обработки собеля... Он портит перекрестье
            Image<Gray, byte> diffScreen = baseFrame.AbsDiff(newFrame);
            Image<Gray, byte> resScreen = new Image<Gray, byte>(new Size(diffScreen.Width, diffScreen.Height));
            CvInvoke.Threshold(diffScreen, resScreen, this.ThresholdValue, 255, Emgu.CV.CvEnum.ThresholdType.Binary); //Выведи константу на форму
            diffScreen.Dispose();
            /*
            var firstFrame = SobelCalculate(newFrame).Convert<Gray, Byte>();
            var lastFrame = SobelCalculate(baseFrame).Convert<Gray, Byte>();
            Image<Gray, byte> diffScreen = lastFrame.AbsDiff(firstFrame);
            Image<Gray, byte> resScreen = new Image<Gray, byte>(new Size(diffScreen.Width, diffScreen.Height));
            CvInvoke.Threshold(diffScreen, resScreen, 70, 255, Emgu.CV.CvEnum.ThresholdType.Binary); //Выведи константу на форму
            diffScreen.Dispose();
            */
            return resScreen;
        }
    }
}
