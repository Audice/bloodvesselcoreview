﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.EventsArgs
{
    public class ExceptionEventArgs: EventArgs
    {
        public string ExceptionMessage { get; set; }
        public string ExceptionStackTrace { get; set; }
    }
}
