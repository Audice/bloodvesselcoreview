﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.EventsArgs
{
    public class FrameReadyEventArgs: EventArgs
    {
        /// <summary>
        /// Изображение в оттенках серого, для детектирования движения
        /// </summary>
        public Image<Gray, Byte> Gray_Frame { get; set; }
        /// <summary>
        /// Чёрно-белое изображение, с профелем катетера
        /// </summary>
        public Image<Gray, Byte> Sobel_Frame { get; set; }

        public ushort Framerate { get; set; }
    }
}
