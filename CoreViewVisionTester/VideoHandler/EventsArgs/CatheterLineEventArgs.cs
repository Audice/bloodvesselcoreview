﻿using CoreViewVisionTester.VideoHandler.CatheterModel;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.EventsArgs
{
    public class CatheterLineEventArgs : EventArgs
    {
        public CatheterPoint StartPoint { get; set; }
        public CatheterPoint EndPoint { get; set; }
        public List<CatheterPoint> CatheterPoints { get; set; }
        /// <summary>
        /// Фрейм, который демонстрирует остаток точек после проведения процедуры поиска катетера
        /// </summary>
        public Image<Gray, Byte> AfterCatheterSearchingFrame { get; set; }
    }
}
