﻿using CoreViewVisionTester.VideoHandler.CatheterModel;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.EventsArgs
{
    public class CatheterSkeletEventArgs : EventArgs
    {
        /// <summary>
        /// Начальная точка скелета катетера. Если она равна null, то катетера нет в поле видимости
        /// </summary>
        public CatheterPoint StartSkeletPoint { get; set; }
        /// <summary>
        /// Конечная точка скелета катетера. Является началом сложного хвоста катетера
        /// </summary>
        public CatheterPoint EndSkeletPoint { get; set; }
        /// <summary>
        /// Набор точек, характеризующий скелет катетера
        /// </summary>
        public List<CatheterPoint> CatetherSkeletLine { get; set; }
        /// <summary>
        /// Белый профиль катетера, на чёрном фоне для точного детектирования
        /// </summary>
        public Image<Gray, Byte> SkeletonProfile { get; set; }
    }
}
