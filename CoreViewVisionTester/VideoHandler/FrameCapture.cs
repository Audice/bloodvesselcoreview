﻿using CoreViewVisionTester.VideoHandler.EventsArgs;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler
{
    public class FrameCapture
    {
        /// <summary>
        /// Объект для работы с видеопотоком
        /// </summary>
        private VideoCapture VideoCapture;
        /// <summary>
        /// Идентификатор камеры
        /// </summary>
        public short CameraId { get; private set; }

        public bool IsCaptureFailed { get; private set; }


        public event EventHandler<ExceptionEventArgs> ExceptionRiseEvent;
        protected virtual void OnExceptionRise(ExceptionEventArgs e)
        {
            EventHandler<ExceptionEventArgs> handler = ExceptionRiseEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public bool IsCameraConfigured { get; private set; } = false;


        private bool isCaptureInProgress = false;
        /// <summary>
        /// Флаг, характеризующий состояние процесса считывания кадров с камеры. True - процесс считывания идёт, false - процесс считывания прекращён
        /// </summary>
        public bool IsCaptureInProgress { 
            get { return isCaptureInProgress; } 
            private set
            {
                isCaptureInProgress = value;
                CaptureProcessStateChange?.Invoke(this, EventArgs.Empty);
            }
        }
        public event EventHandler CaptureProcessStateChange;


        //Обработчик события появления нового кадра
        public event EventHandler<FrameReadyEventArgs> FrameReadyEvent;
        protected virtual void OnFrameReady(FrameReadyEventArgs e)
        {
            EventHandler<FrameReadyEventArgs> handler = FrameReadyEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public FrameCapture(short cameraId)
        {
            IsCaptureFailed = true;
            this.VideoCapture = null;
            CheckCameraId(cameraId);
        }
        private void CheckCameraId(short cameraId)
        {
            if (cameraId >= 0)
                CameraId = cameraId;
            else
                CameraId = -1;
        }

        public void VideoCaptureStart()
        {
            if (this.CameraId >= 0)
            {
                if (VideoCapture == null)
                {
                    try
                    {
                        VideoCapture = new VideoCapture(CameraId);
                        IsCaptureFailed = false;
                    }
                    catch (NullReferenceException exception)
                    {
                        ExceptionEventGenerate(exception.Message, exception.StackTrace);
                        IsCaptureFailed = true;
                        IsCaptureInProgress = false;
                    }
                }
                if (VideoCapture != null)
                {
                    if (IsCaptureInProgress)
                    {
                        VideoCapture.ImageGrabbed -= ProcessFrame;
                        VideoCapture.Stop();
                    }
                    else
                    {
                        VideoCapture.ImageGrabbed += ProcessFrame;
                        VideoCapture.Start();
                    }
                    IsCaptureInProgress = !IsCaptureInProgress;
                }
            }
            else
            {
                StopVideoCapture();
            }
        }



        /// <summary>
        /// Остановка стрима
        /// </summary>
        private void StopVideoCapture()
        {
            if (IsCaptureInProgress)
                VideoCapture.ImageGrabbed -= ProcessFrame;
            IsCaptureInProgress = !IsCaptureInProgress;
            VideoCapture.Stop();
        }

        private void ExceptionEventGenerate(string exceptionMessage, string exceptionStackTrace)
        {
            ExceptionEventArgs args = new ExceptionEventArgs() { 
                ExceptionMessage = exceptionMessage, ExceptionStackTrace = exceptionStackTrace 
            };
            this.OnExceptionRise(args);
        }

        private void ProcessFrame(object sender, EventArgs arg)
        {
            if (this.IsCameraConfigured)
            {
                try
                {
                    Mat interimFrame = new Mat();
                    VideoCapture.Retrieve(interimFrame, 0);
                    Image<Gray, Byte> curImage = interimFrame.ToImage<Gray, Byte>();
                    //Оповещаем подписчиков о появлении нового кадра
                    FrameReadyEventArgs args = new FrameReadyEventArgs();
                    args.Gray_Frame = curImage;
                    this.OnFrameReady(args);
                }
                catch (Exception exception)
                {
                    ExceptionEventGenerate(exception.Message, exception.StackTrace);
                }
            }
            else
            {
                Thread.Sleep(5000);
                this.IsCameraConfigured = true;
            }
        }
    }
}
