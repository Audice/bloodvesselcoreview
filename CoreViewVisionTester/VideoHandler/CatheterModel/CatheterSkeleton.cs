﻿using CoreViewVisionTester.VideoHandler.EventsArgs;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.CatheterModel
{
    //Возможно стоит отказаться от optical flow

    public class CatheterSkeleton
    {
        //Переменные для обработки 
        private readonly int BaseMatrixStep = 100;
        private readonly ushort MaxMatrixStep = 400;

        /// <summary>
        /// Размер области очистки исследованного катетера
        /// </summary>
        private byte CleaningAreaSize = 0;



        /// <summary>
        /// Область, в рамках которой производится поиск стартовой точки
        /// </summary>
        private readonly byte StartPointFindsArea = 20;

        private CatheterPoint StartSkelet = null;

        public event EventHandler<CatheterSkeletEventArgs> CatheterSkeletChangeEvent;
        protected virtual void OnCatheterSkeletChange(CatheterSkeletEventArgs e)
        {
            EventHandler<CatheterSkeletEventArgs> handler = CatheterSkeletChangeEvent;
            if (handler != null)
                handler(this, e);
        }

        public event EventHandler<ModernCatheterSkeletEventArgs> ModernCatheterSkeletChangeEvent;
        protected virtual void OnModernCatheterSkeletChange(ModernCatheterSkeletEventArgs e)
        {
            EventHandler<ModernCatheterSkeletEventArgs> handler = ModernCatheterSkeletChangeEvent;
            if (handler != null)
                handler(this, e);
        }

        public CatheterSkeleton(FrameHandler frameHandler)
        {
            //frameHandler.FrameReadyEvent += FindSkeletonPoints; //Рабочий вариант
            //Тестовый вариант
            frameHandler.FrameReadyEvent += ModernFindSkeletonPoints;
        }

        void ModernFindSkeletonPoints(object sender, FrameReadyEventArgs e)
        {
            Image<Gray, Byte> frame = new Image<Gray, byte>(e.Gray_Frame.Data);
            byte profileWidth = (byte)this.BaseMatrixStep; //Использовать для уточнения области удаления
            var localStartCatheter = FindStartPoint(ref profileWidth, e.Gray_Frame);
            if (localStartCatheter != null)
            {
                this.StartSkelet = localStartCatheter;
                if (profileWidth > this.CleaningAreaSize)
                    this.CleaningAreaSize = profileWidth;
            }
            CatheterSkeletEventArgs args = new CatheterSkeletEventArgs();
            List<CatheterPoint> skeletPoints = new List<CatheterPoint>();
            //Если катетр имеет начало - значит возможно он есть
            if (this.StartSkelet != null)
            {
                //Запуск поиска течек в окрестности
                try
                {
                    byte behaviorMode = 0;
                    int coefDuplex = 1;
                    ushort MatrixSize = (ushort)(this.BaseMatrixStep * coefDuplex);
                    skeletPoints = new List<CatheterPoint>();
                    skeletPoints.Add(this.StartSkelet);
                    CatheterPoint nextCatheterPoint = this.StartSkelet;
                    CatheterPoint currentCatheterPoint = this.StartSkelet;
                    int criticalStep = 0;
                    while (currentCatheterPoint != null && criticalStep < 100)
                    {
                        nextCatheterPoint = MatrixSeparation_NextSkeletPoint(ref behaviorMode, MatrixSize, currentCatheterPoint, frame);
                        if (nextCatheterPoint == null)
                        {
                            coefDuplex++;
                            MatrixSize = (ushort)(this.BaseMatrixStep * coefDuplex);
                            if (MatrixSize > this.MaxMatrixStep)
                                break;
                        }
                        else
                        {
                            skeletPoints.Add(new CatheterPoint(nextCatheterPoint));
                            currentCatheterPoint = new CatheterPoint(nextCatheterPoint);
                        }
                        criticalStep++;
                    }
                }
                catch (Exception ex)
                {
                    skeletPoints = null;
                    string exceptionMessage = ex.Message;
                }

                if (skeletPoints != null && skeletPoints.Count > 1)
                {
                    this.OnModernCatheterSkeletChange(CostructModernCatheterSkeletonEventArgs(this.StartSkelet,
                        skeletPoints[skeletPoints.Count - 1], skeletPoints, e.Sobel_Frame, CleaningAreaSize));
                }
                else
                {
                    this.OnModernCatheterSkeletChange(CostructModernCatheterSkeletonEventArgs(this.StartSkelet, null, null, e.Sobel_Frame, CleaningAreaSize));
                }
            }
            else
            {
                this.OnModernCatheterSkeletChange(CostructModernCatheterSkeletonEventArgs(null, null, null, e.Sobel_Frame, CleaningAreaSize));
            }
        }

        /// <summary>
        /// Формирование пакета события изменения скелета
        /// </summary>
        /// <param name="startPoint">Стартовая точка скелета</param>
        /// <param name="endPoint">Конечная точка скелета</param>
        /// <param name="skeletPoints">Список точек скелета</param>
        /// <param name="frame">Кадр с выделенными границами катетера</param>
        /// <returns>Пакет CatheterSkeletEventArgs</returns>
        CatheterSkeletEventArgs CostructCatheterSkeletonEventArgs(CatheterPoint startPoint, CatheterPoint endPoint, 
            List<CatheterPoint> skeletPoints, Image<Gray, Byte> frame)
        {
            return new CatheterSkeletEventArgs()
            {
                CatetherSkeletLine = skeletPoints,
                EndSkeletPoint = endPoint,
                SkeletonProfile = frame,
                StartSkeletPoint = startPoint
            };
        }

        ModernCatheterSkeletEventArgs CostructModernCatheterSkeletonEventArgs(CatheterPoint startPoint, CatheterPoint endPoint,
    List<CatheterPoint> skeletPoints, Image<Gray, Byte> frame, byte cleanAreaSize)
        {
            return new ModernCatheterSkeletEventArgs()
            {
                CatetherSkeletLine = skeletPoints,
                EndSkeletPoint = endPoint,
                SkeletonProfile = frame,
                StartSkeletPoint = startPoint,
                CleaningAreaSize = cleanAreaSize
            };
        }



        /// <summary>
        /// Метод расчёта границ матрицы для поиска точек
        /// </summary>
        /// <param name="currentPoint">Текущая рассматриваемая точка</param>
        /// <param name="behavior">0 - точка справа, 1 - точка сверху, 2 - точка справа, 3 - точка снизу</param>
        /// <param name="frameHeight">Высота фрейма</param>
        /// <param name="frameWidth">Ширина фрейма</param>
        /// <param name="templateSize">Размер рассматриваемой матрицы</param>
        /// <param name="top">Верхняя координата рассмотрения матрицы в фрейме</param>
        /// <param name="bottom">Нижняя координата рассмотрения матрицы в фрейме</param>
        /// <param name="right">Правая координата рассмотрения матрицы в фрейме</param>
        /// <param name="left">Левая координата рассмотрения матрицы в фрейме</param>
        void CalcMatrixRange(CatheterPoint currentPoint, byte behavior, int frameHeight, int frameWidth, ushort templateSize,
            ref ushort top, ref ushort bottom, ref ushort right, ref ushort left)
        {
            short rightBorder = 1;
            short topBorder = 1;
            short bottomBorder = 1;
            short leftBorder = 1;
            switch (behavior)
            {
                case 0:
                    //Формируем верхнюю границу
                    topBorder = (short)(currentPoint.Y - templateSize);
                    if (topBorder < 0) topBorder = 0;
                    top = (ushort)topBorder;
                    //Формируем правую границу
                    rightBorder = (short)(currentPoint.X + templateSize);
                    if (rightBorder >= frameWidth) rightBorder = (short)(frameWidth - 1);
                    right = (ushort)rightBorder;
                    //Формируем нижнюю границу
                    bottomBorder = (short)(currentPoint.Y + templateSize);
                    if (bottomBorder >= frameHeight) bottomBorder = (short)(frameHeight - 1);
                    bottom = (ushort)bottomBorder;
                    //Формируем левую границу
                    left = currentPoint.X;
                    break;
                case 1:
                    //Формируем верхнюю границу
                    top = currentPoint.Y;
                    //Формируем правую границу
                    rightBorder = (short)(currentPoint.X + templateSize);
                    if (rightBorder >= frameWidth) rightBorder = (short)(frameWidth - 1);
                    right = (ushort)rightBorder;
                    //Формируем нижнюю границу
                    bottomBorder = (short)(currentPoint.Y + templateSize);
                    if (bottomBorder >= frameHeight) bottomBorder = (short)(frameHeight - 1);
                    bottom = (ushort)bottomBorder;
                    //Формируем левую границу
                    leftBorder = (short)(currentPoint.X - templateSize);
                    if (leftBorder < 0) leftBorder = 0;
                    left = (ushort)leftBorder;
                    break;
                case 2:
                    //Формируем верхнюю границу
                    topBorder = (short)(currentPoint.Y - templateSize);
                    if (topBorder < 0) topBorder = 0;
                    top = (ushort)topBorder;
                    //Формируем правую границу
                    right = currentPoint.X;
                    //Формируем нижнюю границу
                    bottomBorder = (short)(currentPoint.Y + templateSize);
                    if (bottomBorder >= frameHeight) bottomBorder = (short)(frameHeight - 1);
                    bottom = (ushort)bottomBorder;
                    //Формируем левую границу
                    leftBorder = (short)(currentPoint.X - templateSize);
                    if (leftBorder < 0) leftBorder = 0;
                    left = (ushort)leftBorder;
                    break;
                case 3:
                    //Формируем верхнюю границу
                    topBorder = (short)(currentPoint.Y - templateSize);
                    if (topBorder < 0) topBorder = 0;
                    top = (ushort)topBorder;
                    //Формируем правую границу
                    rightBorder = (short)(currentPoint.X + templateSize);
                    if (rightBorder >= frameWidth) rightBorder = (short)(frameWidth - 1);
                    right = (ushort)rightBorder;
                    //Формируем нижнюю границу
                    bottom = currentPoint.Y;
                    //Формируем левую границу
                    leftBorder = (short)(currentPoint.X - templateSize);
                    if (leftBorder < 0) leftBorder = 0;
                    left = (ushort)leftBorder;
                    break;
            }
        }

        byte GetNewBehavior(byte prevBehavior, CatheterPoint currentPoint, CatheterPoint nextPoint,
            ushort top, ushort bottom, ushort right, ushort left)
        {
            byte resultBehavior = 0;
            //Возможны проблемы на границе фрейма
            int rightDistance = Math.Abs(right - nextPoint.X);
            int leftDistance = Math.Abs(nextPoint.X - left);
            int topDistance = Math.Abs(nextPoint.Y - top);
            int bottomDistance = Math.Abs(bottom - nextPoint.Y);

            List<int> values = null;
            List<byte> behaviors = null;
            int indexMinDistance = 0;
            switch (prevBehavior)
            {
                //Точка находится слева, leftdistance не рассматривается
                case 0:
                    values = new List<int>() { topDistance, rightDistance, bottomDistance };
                    behaviors = new List<byte>() { 3, 0, 1 };               
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
                case 1:
                    values = new List<int>() { leftDistance, rightDistance, bottomDistance };
                    behaviors = new List<byte>() { 2, 0, 1 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
                case 2:
                    values = new List<int>() { leftDistance, topDistance, bottomDistance };
                    behaviors = new List<byte>() { 2, 3, 1 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
                case 3:
                    values = new List<int>() { leftDistance, topDistance, rightDistance };
                    behaviors = new List<byte>() { 2, 3, 0 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
            }
            return resultBehavior;
        }

        void ClearUsedPixels(Image<Gray, Byte> frame, byte nextBehavior, CatheterPoint nextPoint,
            ushort top, ushort bottom, ushort right, ushort left)
        {
            switch (nextBehavior)
            {
                case 0:
                    ClearFrameArea(frame, left, nextPoint.X, top, bottom);
                    break;
                case 1:
                    ClearFrameArea(frame, right, left, top, nextPoint.Y);
                    break;
                case 2:
                    ClearFrameArea(frame, nextPoint.X, right, top, bottom);
                    break;
                case 3:
                    ClearFrameArea(frame, right, left, nextPoint.Y, bottom);
                    break;
            }
        }

        void ClearFrameArea(Image<Gray, Byte> frame, ushort right, ushort left, ushort top, ushort bottom)
        {
            //Удаление точек по оси X от точки старта до следующего, максимально удалённого от исходного элемента
            for (int i = right; i < left; i++)
            {
                for (int j = top; j < bottom; j++)
                {
                    if (j < frame.Data.GetLength(0) && j > 0 && i < frame.Data.GetLength(1) && i > 0)
                    {
                        frame.Data[j, i, 0] = 0;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="behavior">Поведение поиска следующей точки: 0 - точка слева, а область справа, снизу и сверху;</param>
        /// <param name="templateSize"></param>
        /// <param name="currentPoint"></param>
        /// <param name="grayFrame"></param>
        /// <returns></returns>
        private CatheterPoint MatrixSeparation_NextSkeletPoint(ref byte behavior, ushort templateSize, CatheterPoint currentPoint, Image<Gray, Byte> grayFrame)
        {
            int currentTemplateSize = templateSize;
            List<CatheterPoint> vectorizePoints = new List<CatheterPoint>();
            //Установка размров для исследования матрицы
            //Зависит от поведения
            ushort topBorder = 0;
            ushort rightBorder = 0;
            ushort bottomBorder = 0;
            ushort leftBorder = 0;
            CalcMatrixRange(currentPoint, behavior, grayFrame.Data.GetLength(0), grayFrame.Data.GetLength(1), templateSize,
                ref topBorder, ref bottomBorder, ref rightBorder, ref leftBorder);
            //Добавляем ненулевые точки в список
            for (ushort i = topBorder; i < bottomBorder; i++)
            {
                for (ushort j = leftBorder; j < rightBorder; j++)
                {
                    if (grayFrame.Data[i, j, 0] > 0 && grayFrame.Data[i, j, 0] < 100)
                    {
                        vectorizePoints.Add(new CatheterPoint(j, i, 0));
                    }
                }
            }
            //В ходе работы метода создаётся новая точка
            CatheterPoint nextPoint = GetNextSkeletPoint(currentPoint, vectorizePoints);
            //Очищаем память
            vectorizePoints.Clear(); vectorizePoints = null;
            if (nextPoint != null)
            {
                //Определяем следующий тип поведения
                behavior = GetNewBehavior(behavior, currentPoint, nextPoint, topBorder, bottomBorder, rightBorder, leftBorder);
                //Очистить использованные данные
                ClearUsedPixels(grayFrame, behavior, nextPoint, topBorder, bottomBorder, rightBorder, leftBorder);
            }


            return nextPoint;
        }

        /// <summary>
        /// Поиск следующей точки по максимальному отдалению от текущей
        /// </summary>
        /// <param name="curPoint">Текущая точка</param>
        /// <param name="points">Отобранные точки</param>
        /// <returns>Следующая точка</returns>
        private CatheterPoint GetNextSkeletPoint(CatheterPoint curPoint, List<CatheterPoint> points)
        {
            if (points != null && points.Count > 0)
            {
                double maximumDistance = Math.Sqrt(Math.Pow(curPoint.X - points[0].X, 2) 
                                                    + Math.Pow(curPoint.Y - points[0].Y, 2) 
                                                    + Math.Pow(curPoint.Z - points[0].Z, 2));
                CatheterPoint point = new CatheterPoint(points[0]);
                for (int i = 1; i < points.Count; i++)
                {
                    double curDistance = Math.Sqrt(Math.Pow(curPoint.X - points[i].X, 2)
                                                    + Math.Pow(curPoint.Y - points[i].Y, 2)
                                                    + Math.Pow(curPoint.Z - points[i].Z, 2));
                    if (curDistance > maximumDistance)
                    {
                        maximumDistance = curDistance;
                        point = new CatheterPoint(points[i]);
                    }
                }
                return point;
            }
            return null;
        }
        private int SearchNextSkeletFace(CatheterPoint curentPoint, CatheterPoint nextPoint, int templateSize)
        {
            int rightBorder = curentPoint.X + templateSize;
            int leftBorder = curentPoint.X - templateSize;
            int topBorder = curentPoint.Y - templateSize;
            int bottomBorder = curentPoint.Y + templateSize;

            int rightDistance = rightBorder - nextPoint.X;
            int leftDistance = nextPoint.X - leftBorder;
            int topDistance = nextPoint.Y - topBorder;
            int bottomDistance = bottomBorder - nextPoint.Y;

            List<int> array = new List<int>() { leftDistance, topDistance, rightDistance, bottomDistance };
            int indexMinDistance = array.FindIndex(x => x == array.Min());

            return indexMinDistance;
        }
        CatheterPoint FindStartPoint(ref byte profileWidth, Image<Gray, Byte> grayFrame)
        {
            for (ushort i = 0; i < this.StartPointFindsArea; i++)
                for (ushort j = 0; j < grayFrame.Rows - 2; j++)
                {
                    if (grayFrame.Data[j, i, 0] > 0 && grayFrame.Data[j + 1, i, 0] > 0 && grayFrame.Data[j + 2, i, 0] > 0)
                    {
                        FindingProfileWidth(ref profileWidth, grayFrame, i, j);
                        return new CatheterPoint(i, j, 0);
                    }
                }
            return null;
        }

        bool FindingProfileWidth(ref byte profileWidth, Image<Gray, Byte> grayFrame, ushort x_Coord, ushort yy_Coord)
        {
            //Создаём массив для поиска разности контрастов
            byte[,] byteMask = new byte[10, 3];
            double dominateContrast = 0;
            bool isStartProfile = false;
            bool isEndProfile = false;
            ushort startProfile = 0;
            ushort endProfile = 0;
            for (ushort k = 0; k < grayFrame.Data.GetLength(0) - byteMask.GetLength(0); k += 10)
            {
                for (ushort i = 0; i < byteMask.GetLength(0); i++)
                {
                    for (ushort j = 0; j < byteMask.GetLength(1); j++)
                    {
                        byteMask[i, j] = grayFrame.Data[k + i, x_Coord + j, 0];
                    }
                }
                dominateContrast = ContrastAssessment(byteMask, 0);
                
                if (dominateContrast < 0.2)
                {
                    isStartProfile = true;
                    startProfile = k;
                    break;
                }
                ClearArray(byteMask);

            }
            if (isStartProfile)
            {
                for (ushort k = startProfile; k < grayFrame.Data.GetLength(0) - byteMask.GetLength(0); k += 2)
                {
                    for (ushort i = 0; i < byteMask.GetLength(0); i++)
                    {
                        for (ushort j = x_Coord; j < byteMask.GetLength(1); j++)
                        {
                            byteMask[i, j] = grayFrame.Data[k + i, x_Coord + j, 0];
                        }
                    }
                    dominateContrast = ContrastAssessment(byteMask, 0);
                    
                    if (dominateContrast > 0.9)
                    {
                        isEndProfile = true;
                        endProfile = (ushort)(k + 2);
                        break;
                    }

                    ClearArray(byteMask);
                }
                if (isEndProfile)
                {
                    //Константное значение исправь
                    if (endProfile - startProfile < 100)
                        profileWidth = (byte)(endProfile - startProfile);
                    else
                        profileWidth = 0;

                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Оценка контрастности байтовой маски
        /// </summary>
        /// <param name="byteMask"></param>
        /// <param name="targetСontrast"></param>
        /// <returns></returns>
        private double ContrastAssessment(byte[,] byteMask, byte targetСontrast)
        {
            int countTargetContrast = 0;
            byte curTargetСontrast = targetСontrast == 0 ? (byte)0 : (byte)1;
            for (int i = 0; i < byteMask.GetLength(0); i++)
            {
                for (int j = 0; j < byteMask.GetLength(1); j++)
                {
                    if (curTargetСontrast == byteMask[i, j])
                        countTargetContrast++;
                }
            }
            return (double)countTargetContrast / (byteMask.GetLength(0) * byteMask.GetLength(1));
        }

        private void ClearArray(byte[,] byteMask)
        {
            for (int i=0; i < byteMask.GetLength(0); i++)
            {
                for (int j=0; j < byteMask.GetLength(1); j++)
                {
                    byteMask[i, j] = 0;
                }
            }
        }
    }
}
