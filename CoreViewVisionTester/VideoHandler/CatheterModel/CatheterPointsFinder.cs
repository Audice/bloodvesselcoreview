﻿using CoreViewVisionTester.VideoHandler.EventsArgs;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.CatheterModel
{
    public abstract class CatheterPointsFinder
    {
        //Переменные для обработки 
        protected readonly int BaseMatrixStep = 15;
        protected readonly ushort MaxMatrixStep = 75;

        /*
        protected virtual void FindSkeletonPoints(object sender, FrameReadyEventArgs e)
        {
            var frame = new Image<Gray, byte>(e.Gray_Frame.Data);
            var localStartCatheter = FindStartPoint(frame);
            if (localStartCatheter != null)
            {
                this.StartSkelet = localStartCatheter;
            }
            CatheterSkeletEventArgs args = new CatheterSkeletEventArgs();
            List<CatheterPoint> skeletPoints = new List<CatheterPoint>();
            //Если катетр имеет начало - значит возможно он есть
            if (this.StartSkelet != null)
            {
                //Запуск поиска течек в окрестности
                try
                {
                    int behaviorMode = 0;
                    int coefDuplex = 1;
                    int MatrixSize = 100 * coefDuplex;
                    skeletPoints = new List<CatheterPoint>();
                    skeletPoints.Add(this.StartSkelet);
                    CatheterPoint nextCatheterPoint = this.StartSkelet;
                    CatheterPoint currentCatheterPoint = this.StartSkelet;
                    int criticalStep = 0;
                    while (currentCatheterPoint != null && criticalStep < 100)
                    {
                        nextCatheterPoint = NextSkeletPoint(ref behaviorMode, MatrixSize, currentCatheterPoint, frame);
                        if (nextCatheterPoint == null)
                        {
                            coefDuplex++;
                            MatrixSize = 100 * coefDuplex;
                            if (MatrixSize > 301)
                                break;
                        }
                        else
                        {
                            skeletPoints.Add(new CatheterPoint(nextCatheterPoint));
                            currentCatheterPoint = new CatheterPoint(nextCatheterPoint);
                        }
                        criticalStep++;
                    }
                }
                catch (Exception ex)
                {
                    skeletPoints = null;
                    string exceptionMessage = ex.Message;
                }

                if (skeletPoints != null && skeletPoints.Count > 1)
                {
                    this.OnCatheterSkeletChange(CostructCatheterSkeletonEventArgs(this.StartSkelet,
                        skeletPoints[skeletPoints.Count - 1], skeletPoints, e.Sobel_Frame));
                }
                else
                {
                    this.OnCatheterSkeletChange(CostructCatheterSkeletonEventArgs(this.StartSkelet, null, null, e.Sobel_Frame));
                }
            }
            else
            {
                this.OnCatheterSkeletChange(CostructCatheterSkeletonEventArgs(null, null, null, e.Sobel_Frame));
            }
        }

                private CatheterPoint NextSkeletPoint(ref int behavior, int templateSize, CatheterPoint currentPoint, Image<Gray, Byte> grayFrame)
        {
            Image<Gray, Byte> frame = new Image<Gray, byte>(grayFrame.Data);

            CatheterPoint nextPoint = null;
            int currentTemplateSize = templateSize;
            List<CatheterPoint> vectorizePoints = new List<CatheterPoint>();

            short topBorder = (short)(currentPoint.Y - currentTemplateSize);
            if (topBorder < 0) topBorder = 0;
            short rightBorder = (short)(currentPoint.X + currentTemplateSize);
            if (rightBorder >= frame.Data.GetLength(1)) rightBorder = (short)(frame.Data.GetLength(1) - 1);
            short bottomBorder = (short)(currentPoint.Y + currentTemplateSize);
            if (bottomBorder >= frame.Data.GetLength(0)) bottomBorder = (short)(frame.Data.GetLength(0) - 1);
            short leftBorder = (short)(currentPoint.X - currentTemplateSize);
            if (leftBorder < 0) leftBorder = 0;


            switch (behavior)
            {
                case 0:
                    {
                        int i = 1;
                        try
                        {
                            for (i = 1; i < currentTemplateSize; i++)
                            {
                                if (currentPoint.X + i < frame.Data.GetLength(1) && frame.Data[topBorder, (int)(currentPoint.X + i), 0] > 50)
                                {
                                    vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X + i), (ushort)topBorder, 0));
                                }
                                if (currentPoint.X + i < frame.Data.GetLength(1) && frame.Data[bottomBorder, (int)(currentPoint.X + i), 0] > 50)
                                {
                                    vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X + i), (ushort)bottomBorder, 0));
                                }
                                //Тут аккуратнее
                                if (topBorder + 2 * i < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i, rightBorder, 0] > 0)
                                {
                                    vectorizePoints.Add(new CatheterPoint((ushort)rightBorder, (ushort)(topBorder + 2 * i), 0));
                                }
                                if (topBorder + 2 * i + 1 < frame.Data.GetLength(0) && frame.Data[(int)(topBorder + 2 * i + 1), rightBorder, 0] > 50)
                                {
                                    vectorizePoints.Add(new CatheterPoint((ushort)rightBorder, (ushort)(topBorder + 2 * i + 1), 0));
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            string exm = ex.Message;
                        }
                        
                        nextPoint = GetNextSkeletPoint(vectorizePoints);
                        //Формируем модель поведения
                        if (nextPoint != null)
                        {
                            //Удаление
                            for (i = topBorder; i < bottomBorder; i++)
                            {
                                for (int j = currentPoint.X; j < nextPoint.X; j++)
                                {
                                    if (j < frame.Data.GetLength(1))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }

                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = -1; break;
                                case 1:
                                    behavior = 3; break;
                                case 2:
                                    behavior = 0; break;
                                case 3:
                                    behavior = 1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                case 1:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {

                            if (currentPoint.Y + i < frame.Data.GetLength(0) && frame.Data[currentPoint.Y + i, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)leftBorder, (ushort)(currentPoint.Y + i), 0));
                            }

                            if (currentPoint.Y + i < frame.Data.GetLength(0) && frame.Data[currentPoint.Y + i, rightBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)rightBorder, (ushort)(currentPoint.Y + i), 0));
                            }

                            //Тут аккуратнее
                            if (leftBorder + 2 * i < frame.Data.GetLength(1) && frame.Data[bottomBorder, leftBorder + 2 * i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i), (ushort)bottomBorder, 0));
                            }
                            if (leftBorder + 2 * i + 1 < frame.Data.GetLength(1) && frame.Data[bottomBorder, leftBorder + 2 * i + 1, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i + 1), (ushort)bottomBorder, 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);

                        //Формируем модель поведения
                        if (nextPoint != null)
                        {

                            //Удаление
                            for (int i = currentPoint.Y; i < nextPoint.Y; i++)
                            {
                                for (int j = leftBorder; j < rightBorder; j++)
                                {
                                    if (i < frame.Data.GetLength(0))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }
                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = 2; break;
                                case 1:
                                    behavior = -1; break;
                                case 2:
                                    behavior = 0; break;
                                case 3:
                                    behavior = 1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                case 2:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {
                            if (currentPoint.X - i >= 0 && frame.Data[topBorder, currentPoint.X - i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X - i), (ushort)topBorder, 0));
                            }
                            if (currentPoint.X - i >= 0 && frame.Data[bottomBorder, currentPoint.X - i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X - i), (ushort)bottomBorder, 0));
                            }
                            //Тут аккуратнее
                            if (topBorder + 2 * i < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)leftBorder, (ushort)(topBorder + 2 * i), 0));
                            }
                            if (topBorder + 2 * i + 1 < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i + 1, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)leftBorder, (ushort)(topBorder + 2 * i + 1), 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);

                        //Формируем модель поведения
                        if (nextPoint != null)
                        {
                            //Удаление
                            for (int i = topBorder; i < bottomBorder; i++)
                            {
                                for (int j = nextPoint.X; j < currentPoint.X; j++)
                                {
                                    if (j < frame.Data.GetLength(1))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }

                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = 2; break;
                                case 1:
                                    behavior = 3; break;
                                case 2:
                                    behavior = -1; break;
                                case 3:
                                    behavior = 1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                case 3:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {
                            if (currentPoint.Y - i >= 0 && frame.Data[currentPoint.Y - i, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)leftBorder, (ushort)(currentPoint.Y - i), 0));
                            }

                            if (currentPoint.Y - i >= 0 && frame.Data[currentPoint.Y - i, rightBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)rightBorder, (ushort)(currentPoint.Y - i), 0));
                            }

                            //Тут аккуратнее
                            if (leftBorder + 2 * i < frame.Data.GetLength(1) && frame.Data[topBorder, leftBorder + 2 * i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i), (ushort)topBorder, 0));
                            }
                            if (leftBorder + 2 * i + 1 < frame.Data.GetLength(1) && frame.Data[topBorder, leftBorder + 2 * i + 1, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i + 1), (ushort)topBorder, 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);



                        //Формируем модель поведения
                        if (nextPoint != null)
                        {
                            //Удаление
                            for (int i = nextPoint.Y; i < currentPoint.Y; i++)
                            {
                                for (int j = leftBorder; j < rightBorder; j++)
                                {
                                    if (i >= 0 && i < frame.Data.GetLength(0))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }

                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = 2; break;
                                case 1:
                                    behavior = 3; break;
                                case 2:
                                    behavior = 0; break;
                                case 3:
                                    behavior = -1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                default:
                    nextPoint = null;
                    break;
            };

            return nextPoint;
        }
        //step позволяет увеличить число точек и сгладить кривую
        private CatheterPoint NextSkeletPoint(ref int behavior, int templateSize, int step, CatheterPoint currentPoint, Image<Gray, Byte> grayFrame)
        {
            Image<Gray, Byte> frame = new Image<Gray, byte>(grayFrame.Data);

            CatheterPoint nextPoint = null;
            int currentTemplateSize = templateSize;
            List<CatheterPoint> vectorizePoints = new List<CatheterPoint>();

            step = templateSize / 2;

            ushort topBorder = (ushort)(currentPoint.Y - step);
            if (topBorder < 0) topBorder = 0;
            ushort rightBorder = (ushort)(currentPoint.X + step);
            if (rightBorder >= frame.Data.GetLength(1)) rightBorder = (ushort)(frame.Data.GetLength(1) - 1);
            ushort bottomBorder = (ushort)(currentPoint.Y + step);
            if (bottomBorder >= frame.Data.GetLength(0)) bottomBorder = (ushort)(frame.Data.GetLength(0) - 1);
            ushort leftBorder = (ushort)(currentPoint.X - step);
            if (leftBorder < 0) leftBorder = 0;


            switch (behavior)
            {
                case 0:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {
                            if (currentPoint.X + i < frame.Data.GetLength(1) && frame.Data[topBorder, currentPoint.X + i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X + i), topBorder, 0));
                            }
                            if (currentPoint.X + i < frame.Data.GetLength(1) && frame.Data[bottomBorder, currentPoint.X + i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X + i), bottomBorder, 0));
                            }
                            //Тут аккуратнее
                            if (topBorder + 2 * i < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i, rightBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(rightBorder, (ushort)(topBorder + 2 * i), 0));
                            }
                            if (topBorder + 2 * i + 1 < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i + 1, rightBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(rightBorder, (ushort)(topBorder + 2 * i + 1), 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);
                        //Формируем модель поведения
                        if (nextPoint.X >= 0)
                        {
                            //Удаление
                            for (int i = topBorder; i < bottomBorder; i++)
                            {
                                for (int j = currentPoint.X; j < nextPoint.X; j++)
                                {
                                    if (j < frame.Data.GetLength(1))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }

                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = -1; break;
                                case 1:
                                    behavior = 3; break;
                                case 2:
                                    behavior = 0; break;
                                case 3:
                                    behavior = 1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                case 1:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {

                            if (currentPoint.Y + i < frame.Data.GetLength(0) && frame.Data[currentPoint.Y + i, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(leftBorder, (ushort)(currentPoint.Y + i), 0));
                            }

                            if (currentPoint.Y + i < frame.Data.GetLength(0) && frame.Data[currentPoint.Y + i, rightBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(rightBorder, (ushort)(currentPoint.Y + i), 0));
                            }

                            //Тут аккуратнее
                            if (leftBorder + 2 * i < frame.Data.GetLength(1) && frame.Data[bottomBorder, leftBorder + 2 * i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i), bottomBorder, 0));
                            }
                            if (leftBorder + 2 * i + 1 < frame.Data.GetLength(1) && frame.Data[bottomBorder, leftBorder + 2 * i + 1, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i + 1), bottomBorder, 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);

                        //Формируем модель поведения
                        if (nextPoint.X >= 0)
                        {

                            //Удаление
                            for (int i = currentPoint.Y; i < nextPoint.Y; i++)
                            {
                                for (int j = leftBorder; j < rightBorder; j++)
                                {
                                    if (i < frame.Data.GetLength(0))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }
                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = 2; break;
                                case 1:
                                    behavior = -1; break;
                                case 2:
                                    behavior = 0; break;
                                case 3:
                                    behavior = 1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                case 2:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {
                            if (currentPoint.X - i >= 0 && frame.Data[topBorder, currentPoint.X - i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X - i), topBorder, 0));
                            }
                            if (currentPoint.X - i >= 0 && frame.Data[bottomBorder, currentPoint.X - i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(currentPoint.X - i), bottomBorder, 0));
                            }
                            //Тут аккуратнее
                            if (topBorder + 2 * i < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(leftBorder, (ushort)(topBorder + 2 * i), 0));
                            }
                            if (topBorder + 2 * i + 1 < frame.Data.GetLength(0) && frame.Data[topBorder + 2 * i + 1, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(leftBorder, (ushort)(topBorder + 2 * i + 1), 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);

                        //Формируем модель поведения
                        if (nextPoint.X >= 0)
                        {
                            //Удаление
                            for (int i = topBorder; i < bottomBorder; i++)
                            {
                                for (int j = nextPoint.X; j < currentPoint.X; j++)
                                {
                                    if (j < frame.Data.GetLength(1))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }

                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = 2; break;
                                case 1:
                                    behavior = 3; break;
                                case 2:
                                    behavior = -1; break;
                                case 3:
                                    behavior = 1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                case 3:
                    {
                        for (int i = 1; i < currentTemplateSize; i++)
                        {
                            if (currentPoint.Y - i >= 0 && frame.Data[currentPoint.Y - i, leftBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(leftBorder, (ushort)(currentPoint.Y - i), 0));
                            }

                            if (currentPoint.Y - i >= 0 && frame.Data[currentPoint.Y - i, rightBorder, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint(rightBorder, (ushort)(currentPoint.Y - i), 0));
                            }

                            //Тут аккуратнее
                            if (leftBorder + 2 * i < frame.Data.GetLength(1) && frame.Data[topBorder, leftBorder + 2 * i, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i), topBorder, 0));
                            }
                            if (leftBorder + 2 * i + 1 < frame.Data.GetLength(1) && frame.Data[topBorder, leftBorder + 2 * i + 1, 0] > 50)
                            {
                                vectorizePoints.Add(new CatheterPoint((ushort)(leftBorder + 2 * i + 1), topBorder, 0));
                            }
                        }
                        nextPoint = GetNextSkeletPoint(vectorizePoints);



                        //Формируем модель поведения
                        if (nextPoint.X >= 0)
                        {
                            //Удаление
                            for (int i = nextPoint.Y; i < currentPoint.Y; i++)
                            {
                                for (int j = leftBorder; j < rightBorder; j++)
                                {
                                    if (i >= 0 && i < frame.Data.GetLength(0))
                                    {
                                        frame.Data[i, j, 0] = 0;
                                    }
                                }
                            }

                            int newBehavior = SearchNextSkeletFace(currentPoint, nextPoint, currentTemplateSize);
                            switch (newBehavior)
                            {
                                case 0:
                                    behavior = 2; break;
                                case 1:
                                    behavior = 3; break;
                                case 2:
                                    behavior = 0; break;
                                case 3:
                                    behavior = -1; break;
                                default:
                                    behavior = -1; break;
                            }
                        }
                        else
                        {
                            nextPoint = null;
                        }
                        break;
                    }
                default:
                    nextPoint = null;
                    break;
            };

            return nextPoint;
        }

                private CatheterPoint GetNextSkeletPoint(List<CatheterPoint> points)
        {
            int _x = 0;
            int _y = 0;
            if (points != null && points.Count > 0)
            {
                for (int i = 0; i < points.Count; i++)
                {
                    _x += points[i].X;
                    _y += points[i].Y;
                }
                return new CatheterPoint((ushort)(_x / points.Count), (ushort)(_y / points.Count), 0);
            }
            else
            {
                return null;
            }
        }

        */
    }
}
