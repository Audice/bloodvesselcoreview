﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.CatheterModel
{
    public enum PointType { SkeletonPoint, InterpolatedPoint }
    public class CatheterPoint
    {
        public ushort X { get; private set; }
        public ushort Y { get; private set; }
        public ushort Z { get; private set; }

        public ushort[,] StateMatrix { get; set; }

        public PointType Type { get; private set; }

        public CatheterPoint()
        {
            this.X = 0; this.Y = 0; this.Z = 0;
        }

        public CatheterPoint(ushort x, ushort y, ushort z)
        {
            this.X = x; this.Y = y; this.Z = z;
        }

        public CatheterPoint(CatheterPoint catheterPoint)
        {
            if (catheterPoint != null)
            {
                this.X = catheterPoint.X; this.Y = catheterPoint.Y; this.Z = catheterPoint.Z;
            }
            else
            {
                this.X = 0; this.Y = 0; this.Z = 0;
            }  
        }

    }
}
