﻿using CoreViewVisionTester.VideoHandler.EventsArgs;
using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoreViewVisionTester.VideoHandler.CatheterModel
{
    public class CatheterWire
    {
        /// <summary>
        /// Размер области очистки исследованного катетера
        /// </summary>
        private byte CleaningAreaSize = 0;

        /// <summary>
        /// Область, в рамках которой производится поиск стартовой точки
        /// </summary>
        private readonly byte StartPointFindsArea = 20;

        /// <summary>
        /// Поле предназначено для хранения предыдущего профиля катетера, дабы проследить дельту изменения кривой.
        /// Если изменение не большое, то мы не обновляем катетр
        /// </summary>
        private List<CatheterPoint> PreviousCatheterLine = null;

        //Переменные для обработки 
        private readonly ushort MaxMatrixStep = 100;
        

        /// <summary>
        /// Событие, происходящее при обновлении катетера
        /// </summary>
        public event EventHandler<CatheterLineEventArgs> CatheterLineChangeEvent;
        /// <summary>
        /// Инициирование события изменения модели катетера
        /// </summary>
        /// <param name="e">Набор параметров обытия изменения катетера</param>
        protected virtual void OnCatheterLineChange(CatheterLineEventArgs e)
        {
            EventHandler<CatheterLineEventArgs> handler = CatheterLineChangeEvent;
            if (handler != null)
                handler(this, e);
        }


        FrameHandler FrameHandler { get; set; }

        //CatheterSkeleton Skeleton;
        public CatheterWire(FrameHandler frameHandler)//CatheterSkeleton skeleton)
        {
            this.FrameHandler = frameHandler;
            if (this.FrameHandler != null)
                this.FrameHandler.FrameReadyEvent += FrameUpdated;

            //this.Skeleton = skeleton;
            //this.Skeleton.CatheterSkeletChangeEvent += CatheterSkeletUpdated; рабочая
            //this.Skeleton.ModernCatheterSkeletChangeEvent += CatheterSkeletUpdated;
        }

        CatheterPoint StartCatheterPoint = null;

        void FrameUpdated(object sender, FrameReadyEventArgs e)
        {
            //1. Делаем копию текущего фрейма. Фрейм содержит байты со значениями 0 и 255. Где 0 - это фон,
            // а 255 - вероятно катетр или артефакт
            Image<Gray, Byte> currentFrame = new Image<Gray, byte>(e.Sobel_Frame.Data);
            //2. Ширина катетера, нужна для аккуратного обхода катетера
            byte profileWidth = 0;
            //3. Поиск стартовой точки катетера
            var localStartCatheter = FindStartPoint(ref profileWidth, currentFrame);
            
            if (localStartCatheter != null)
            {
                this.StartCatheterPoint = localStartCatheter;
                if (profileWidth > 0)
                    this.CleaningAreaSize = profileWidth;
            }
            else
            {
                return;
            }

            CatheterLineEventArgs args = new CatheterLineEventArgs();
            List<CatheterPoint> skeletPoints = new List<CatheterPoint>();

            if (this.StartCatheterPoint != null)
            {
                //Запуск поиска течек в окрестности
                try
                {
                    byte behaviorMode = 0;
                    int coefDuplex = 1;
                    ushort MatrixSize = (ushort)(this.CleaningAreaSize * coefDuplex);
                    skeletPoints = new List<CatheterPoint>();
                    skeletPoints.Add(this.StartCatheterPoint);
                    CatheterPoint nextCatheterPoint = this.StartCatheterPoint;
                    CatheterPoint currentCatheterPoint = this.StartCatheterPoint;
                    int criticalStep = 0;
                    bool hasCross = false;
                    while (currentCatheterPoint != null && criticalStep < 100)
                    {
                        //nextCatheterPoint = MatrixSeparation_NextSkeletPoint(ref behaviorMode, MatrixSize, currentCatheterPoint, currentFrame);
                        nextCatheterPoint = BorderAnalysisAlgorithm_NextSkeletPoint(ref behaviorMode, MatrixSize, 
                            currentCatheterPoint, currentFrame, ref hasCross);

                        if (nextCatheterPoint == null)
                        {
                            coefDuplex++;
                            MatrixSize = (ushort)(this.CleaningAreaSize * coefDuplex);
                            if (MatrixSize > this.MaxMatrixStep)
                                break;
                        }
                        else
                        {
                            skeletPoints.Add(new CatheterPoint(nextCatheterPoint));
                            currentCatheterPoint = new CatheterPoint(nextCatheterPoint);
                            coefDuplex = 1;
                            MatrixSize = (ushort)(this.CleaningAreaSize * coefDuplex);
                        }
                        criticalStep++;
                    }
                }
                catch (Exception ex)
                {
                    skeletPoints = null;
                    string exceptionMessage = ex.Message;
                }

                if (skeletPoints != null && skeletPoints.Count > 1)
                {
                    this.OnCatheterLineChange(ConstructCatheterLineEventArgs(this.StartCatheterPoint,
                        skeletPoints[skeletPoints.Count - 1], skeletPoints, currentFrame));
                }
                else
                {
                    this.OnCatheterLineChange(ConstructCatheterLineEventArgs(this.StartCatheterPoint, null, null, currentFrame));
                }
            }
            else
            {
                this.OnCatheterLineChange(ConstructCatheterLineEventArgs(null, null, null, currentFrame));
            }
        }

       
        /// <summary>
        /// Конструерование пакета аргументов, описывающих состояние катетера и фрейма на данный момент
        /// </summary>
        /// <param name="startPoint">Начальная точка катетера</param>
        /// <param name="endPoint">Конечная точка катетера</param>
        /// <param name="catheterPoints">Список точек катетера</param>
        /// <param name="remeiningPieces">Фрейм, после нахождения списка точек #Debug</param>
        /// <returns></returns>
        CatheterLineEventArgs ConstructCatheterLineEventArgs(CatheterPoint startPoint, 
            CatheterPoint endPoint, List<CatheterPoint> catheterPoints, Image<Gray, Byte> remeiningPieces)
        {
            return new CatheterLineEventArgs() { CatheterPoints = catheterPoints, StartPoint = startPoint, 
                EndPoint = endPoint, AfterCatheterSearchingFrame = remeiningPieces};
        }

        //Поиск начала катетера
        #region

        /// <summary>
        /// Метод поиска основан на переборе фрейма на соответствие скоплению белых точек более чем 10 процентов от 
        /// размера рассмариваемой области
        /// </summary>
        /// <param name="profileWidth">Размер катетера в пикселях</param>
        /// <param name="grayFrame">Фрейм для поиска начала катетера</param>
        /// <returns>Стартовая точка катетера</returns>
        CatheterPoint FindStartPoint(ref byte profileWidth, Image<Gray, Byte> grayFrame)
        {
            //Создадим матрицу 5х5, для поиска места с повышенной концентрацией белых точек
            byte FrameBlockSize = 5;
            double maxPart = double.MinValue;
            double percentStartFind = 0.1;
            ushort startI = 0, startJ = 0;
            for (ushort i = 0; i < this.StartPointFindsArea; i++)
            {
                for (ushort j = 0; j < grayFrame.Rows - FrameBlockSize; j++)
                {
                    double curPart = 0;
                    for (int rowFB = 0; rowFB < FrameBlockSize; rowFB++)
                    {
                        for (int colFB = 0; colFB < FrameBlockSize; colFB++)
                        {
                            if (grayFrame.Data[j + rowFB, i + colFB, 0] != 0)
                                curPart++;
                        }
                    }
                    curPart = curPart / (FrameBlockSize * FrameBlockSize);
                    if (curPart > percentStartFind && curPart > maxPart) //Если плотность белого в этом блоке больше 20%, то катетр где то здесь
                    {
                        maxPart = curPart;
                        startI = i;
                        startJ = j;
                    }
                }
                if (maxPart != double.MinValue)
                {
                    FindingProfileWidth(ref profileWidth, grayFrame, startI, startJ, FrameBlockSize, percentStartFind);
                    //Удаление не воспринятых строк
                    try
                    {
                        for (int col = 0; col <= startI; col++)
                        {
                            for (int row = startJ - 2 * profileWidth; row < startJ + 2 * profileWidth; row++)
                            {
                                if (row < grayFrame.Rows && row >= 0 && col < grayFrame.Cols)
                                    grayFrame.Data[row, col, 0] = 0;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        string mes = ex.Message;
                    }


                    return new CatheterPoint(startI, startJ, 0);
                }
            }
            return null;
        }

        /// <summary>
        /// Метод поиска толщины катетера
        /// </summary>
        /// <param name="profileWidth">Переменная для возврата толщины катетера</param>
        /// <param name="grayFrame">Фрейм для поиска ширины катетера</param>
        /// <param name="x_Coord">Координата столбца в фрейме для поиска ширины катетера</param>
        /// <param name="yy_Coord">Координата строки в фрейме для поиска ширины катетера</param>
        /// <param name="FrameBlockSize">Размер области поиска скопления точек</param>
        /// <param name="percentStartFind">Процент белых точек, по которому определяется начало катетера</param>
        /// <returns></returns>
        bool FindingProfileWidth(ref byte profileWidth, Image<Gray, Byte> grayFrame, ushort x_Coord,
            ushort yy_Coord, byte FrameBlockSize, double percentStartFind)
        {
            ushort startProfile = yy_Coord;
            ushort endProfile = 0;

            for (ushort k = startProfile; k < grayFrame.Data.GetLength(0) - FrameBlockSize; k++)
            {
                double curPart = 0;
                for (int rowFB = 0; rowFB < FrameBlockSize; rowFB++)
                {
                    for (int colFB = 0; colFB < FrameBlockSize; colFB++)
                    {
                        if (grayFrame.Data[k + rowFB, x_Coord + colFB, 0] == 0)
                            curPart++;
                    }
                }
                curPart = curPart / (FrameBlockSize * FrameBlockSize);
                if (curPart > (1.0 - percentStartFind)) //Если плотность белого в этом блоке больше 20%, то катетр где то здесь
                {
                    endProfile = k;
                    break;
                }
            }
            if (endProfile > 0)
            {
                //Константное значение исправь
                if (endProfile - startProfile < 20)
                    profileWidth = (byte)(endProfile - startProfile + 2); //+2 просто так... возможно лучше без них
                else
                    profileWidth = 0;
                return true;
            }
            return false;
        }

        #endregion

        /// <summary>
        /// Определение близости точек
        /// </summary>
        /// <param name="firstPoint">Первая точка</param>
        /// <param name="secondPoint">Вторая точка</param>
        /// <param name="requiredDistance">Мера близости точек</param>
        /// <returns>True - точки близки, false - иначе</returns>
        bool IsPointVicinity(CatheterPoint firstPoint, CatheterPoint secondPoint, ushort requiredDistance)
        {
            double distance = Math.Sqrt(Math.Pow(firstPoint.X - secondPoint.X, 2)
                + Math.Pow(firstPoint.Y - secondPoint.Y, 2)
                + Math.Pow(firstPoint.Z - secondPoint.Z, 2));
            return requiredDistance > distance;
        }



        /// <summary>
        /// Метод расчёта границ матрицы для поиска точек
        /// </summary>
        /// <param name="currentPoint">Текущая рассматриваемая точка</param>
        /// <param name="behavior">0 - точка справа, 1 - точка сверху, 2 - точка справа, 3 - точка снизу</param>
        /// <param name="frameHeight">Высота фрейма</param>
        /// <param name="frameWidth">Ширина фрейма</param>
        /// <param name="templateSize">Размер рассматриваемой матрицы</param>
        /// <param name="top">Верхняя координата рассмотрения матрицы в фрейме</param>
        /// <param name="bottom">Нижняя координата рассмотрения матрицы в фрейме</param>
        /// <param name="right">Правая координата рассмотрения матрицы в фрейме</param>
        /// <param name="left">Левая координата рассмотрения матрицы в фрейме</param>
        void CalcMatrixRange(CatheterPoint currentPoint, byte behavior, int frameHeight, int frameWidth, ushort templateSize,
            ref ushort top, ref ushort bottom, ref ushort right, ref ushort left)
        {
            short rightBorder = 1;
            short topBorder = 1;
            short bottomBorder = 1;
            short leftBorder = 1;
            switch (behavior)
            {
                case 0:
                    //Формируем верхнюю границу
                    topBorder = (short)(currentPoint.Y - templateSize);
                    if (topBorder < 0) topBorder = 0;
                    top = (ushort)topBorder;
                    //Формируем правую границу
                    rightBorder = (short)(currentPoint.X + templateSize);
                    if (rightBorder >= frameWidth) rightBorder = (short)(frameWidth - 1);
                    right = (ushort)rightBorder;
                    //Формируем нижнюю границу
                    bottomBorder = (short)(currentPoint.Y + templateSize);
                    if (bottomBorder >= frameHeight) bottomBorder = (short)(frameHeight - 1);
                    bottom = (ushort)bottomBorder;
                    //Формируем левую границу
                    left = currentPoint.X;
                    break;
                case 1:
                    //Формируем верхнюю границу
                    top = currentPoint.Y;
                    //Формируем правую границу
                    rightBorder = (short)(currentPoint.X + templateSize);
                    if (rightBorder >= frameWidth) rightBorder = (short)(frameWidth - 1);
                    right = (ushort)rightBorder;
                    //Формируем нижнюю границу
                    bottomBorder = (short)(currentPoint.Y + templateSize);
                    if (bottomBorder >= frameHeight) bottomBorder = (short)(frameHeight - 1);
                    bottom = (ushort)bottomBorder;
                    //Формируем левую границу
                    leftBorder = (short)(currentPoint.X - templateSize);
                    if (leftBorder < 0) leftBorder = 0;
                    left = (ushort)leftBorder;
                    break;
                case 2:
                    //Формируем верхнюю границу
                    topBorder = (short)(currentPoint.Y - templateSize);
                    if (topBorder < 0) topBorder = 0;
                    top = (ushort)topBorder;
                    //Формируем правую границу
                    right = currentPoint.X;
                    //Формируем нижнюю границу
                    bottomBorder = (short)(currentPoint.Y + templateSize);
                    if (bottomBorder >= frameHeight) bottomBorder = (short)(frameHeight - 1);
                    bottom = (ushort)bottomBorder;
                    //Формируем левую границу
                    leftBorder = (short)(currentPoint.X - templateSize);
                    if (leftBorder < 0) leftBorder = 0;
                    left = (ushort)leftBorder;
                    break;
                case 3:
                    //Формируем верхнюю границу
                    topBorder = (short)(currentPoint.Y - templateSize);
                    if (topBorder < 0) topBorder = 0;
                    top = (ushort)topBorder;
                    //Формируем правую границу
                    rightBorder = (short)(currentPoint.X + templateSize);
                    if (rightBorder >= frameWidth) rightBorder = (short)(frameWidth - 1);
                    right = (ushort)rightBorder;
                    //Формируем нижнюю границу
                    bottom = currentPoint.Y;
                    //Формируем левую границу
                    leftBorder = (short)(currentPoint.X - templateSize);
                    if (leftBorder < 0) leftBorder = 0;
                    left = (ushort)leftBorder;
                    break;
            }
        }

        byte GetNewBehavior(byte prevBehavior, CatheterPoint currentPoint, CatheterPoint nextPoint,
            ushort top, ushort bottom, ushort right, ushort left)
        {
            byte resultBehavior = 0;
            //Возможны проблемы на границе фрейма
            int rightDistance = Math.Abs(right - nextPoint.X);
            int leftDistance = Math.Abs(nextPoint.X - left);
            int topDistance = Math.Abs(nextPoint.Y - top);
            int bottomDistance = Math.Abs(bottom - nextPoint.Y);

            List<int> values = null;
            List<byte> behaviors = null;
            int indexMinDistance = 0;
            switch (prevBehavior)
            {
                //Точка находится слева, leftdistance не рассматривается
                case 0:
                    values = new List<int>() { topDistance, rightDistance, bottomDistance };
                    behaviors = new List<byte>() { 3, 0, 1 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
                case 1:
                    values = new List<int>() { leftDistance, rightDistance, bottomDistance };
                    behaviors = new List<byte>() { 2, 0, 1 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
                case 2:
                    values = new List<int>() { leftDistance, topDistance, bottomDistance };
                    behaviors = new List<byte>() { 2, 3, 1 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
                case 3:
                    values = new List<int>() { leftDistance, topDistance, rightDistance };
                    behaviors = new List<byte>() { 2, 3, 0 };
                    indexMinDistance = values.FindIndex(x => x == values.Min()); //Проверь, может MAX поставить для точности
                    resultBehavior = behaviors[indexMinDistance];
                    break;
            }
            return resultBehavior;
        }

        void ClearUsedPixels(Image<Gray, Byte> frame, byte nextBehavior, CatheterPoint nextPoint, CatheterPoint currentPoint,
            ushort top, ushort bottom, ushort right, ushort left)
        {
            ushort curTop = top, curBottom = bottom, curLeft = left, curRight = right;
            switch (nextBehavior)
            {
                case 0:
                    //Идея: делать предрасчёт удаляемой области
                    curTop = top; curBottom = bottom;
                    VerticalDelArea(ref curTop, ref curBottom, frame, currentPoint, nextPoint);
                    ClearFrameArea(frame, left, nextPoint.X, curTop, curBottom);
                    break;
                case 1:
                    curLeft = left; curRight = right;
                    HorizontalDelArea(ref curLeft, ref curRight, frame, currentPoint, nextPoint);
                    ClearFrameArea(frame, curLeft, curRight, top, nextPoint.Y);
                    break;
                case 2:
                    curTop = top; curBottom = bottom;
                    VerticalDelArea(ref curTop, ref curBottom, frame, currentPoint, nextPoint);
                    ClearFrameArea(frame, nextPoint.X, right, curTop, curBottom);
                    break;
                case 3:
                    curLeft = left; curRight = right;
                    HorizontalDelArea(ref curLeft, ref curRight, frame, currentPoint, nextPoint);
                    ClearFrameArea(frame, curLeft, curRight, nextPoint.Y, bottom);
                    break;
            }
        }

        /// <summary>
        /// Метод подсчёта процента тёмных пикселей
        /// </summary>
        /// <param name="frame">Кадр в котором производится оценка затемнённости области</param>
        /// <param name="startX"></param>
        /// <param name="startY"></param>
        /// <param name="finishX"></param>
        /// <param name="finishY"></param>
        /// <returns>Процент затёмнённости области: от 0 до 1</returns>
        double CalcPercentDarkArea(Image<Gray, Byte> frame, ushort startX, ushort startY, ushort finishX, ushort finishY)
        {
            double countDarkPixels = 0;
            ushort countAreasPixels = (ushort)(Math.Abs(finishX - startX) * Math.Abs(finishY - startY));
            for (int i = startX; i < finishX; i++)
            {
                for (int j = startY; j < finishY; j++)
                {
                    if (frame.Data[j, i, 0] == 0)
                        countDarkPixels++;
                }
            }
            return countDarkPixels / countAreasPixels;
        }
        /// <summary>
        /// Метод расчёта удаляемой области для поведения 0 и 2
        /// </summary>
        void VerticalDelArea(ref ushort top, ref ushort bottom, Image<Gray, Byte> frame, CatheterPoint currentPoint, CatheterPoint nextPoint)
        {
            ushort curTop = 0;
            ushort curBottom = 0;
            if (currentPoint.Y > nextPoint.Y)
            {
                curBottom = currentPoint.Y;
                curTop = nextPoint.Y;
            }
            else
            {
                curBottom = nextPoint.Y;
                curTop = currentPoint.Y;
            }
            //Определяем стартовую и финишную точку по горизонтале
            ushort curLeft = 0;
            ushort curRight = 0;
            if (currentPoint.X > nextPoint.X)
            {
                curLeft = nextPoint.X;
                curRight = currentPoint.X;
            }
            else
            {
                curLeft = currentPoint.X;
                curRight = nextPoint.X;
            }
            //Теперь ищем точку, на которой процент темноты областей сверху области равен 100%. Пусть высота матрицы равна 3 пикселя
            ushort areaHeight = 3;
            short heightIndex = 0;
            double darkPercent = 0;
            while (darkPercent < 0.99 && (curTop - areaHeight - heightIndex) > 0)
            {
                darkPercent = CalcPercentDarkArea(frame, curLeft, (ushort)(curTop - areaHeight - heightIndex), curRight, (ushort)(curTop - heightIndex));
                heightIndex++;
            }
            top = curTop - areaHeight - heightIndex >= 0 ? (ushort)(curTop - areaHeight - heightIndex) : (ushort)0;
            //Теперь ищем точку, на которой процент темноты областей снизу области равен 100%. Пусть высота матрицы равна 3 пикселя
            heightIndex = 0;
            darkPercent = 0;
            while (darkPercent < 0.99 && (curTop + areaHeight + heightIndex) < frame.Data.GetLength(0))
            {
                darkPercent = CalcPercentDarkArea(frame, curLeft, (ushort)(curTop + heightIndex), 
                    curRight, (ushort)(curTop + areaHeight + heightIndex));
                heightIndex++;
            }
            bottom = curBottom + areaHeight + heightIndex >= frame.Data.GetLength(0) ? 
                (ushort)(frame.Data.GetLength(0) - 1) : (ushort)(curBottom + areaHeight + heightIndex);

        }
        /// <summary>
        /// Метод расчёта удаляемой области для поведения 1 и 3
        /// </summary>
        void HorizontalDelArea(ref ushort left, ref ushort right, Image<Gray, Byte> frame, CatheterPoint currentPoint, CatheterPoint nextPoint)
        {
            //Определяем стартовую и финишную точку по горизонтале
            ushort curLeft = 0;
            ushort curRight = 0;
            if (currentPoint.X > nextPoint.X)
            {
                curLeft = nextPoint.X;
                curRight = currentPoint.X;
            }
            else
            {
                curLeft = currentPoint.X;
                curRight = nextPoint.X;
            }
            ushort curTop = 0;
            ushort curBottom = 0;
            if (currentPoint.Y > nextPoint.Y)
            {
                curBottom = currentPoint.Y;
                curTop = nextPoint.Y;
            }
            else
            {
                curBottom = nextPoint.Y;
                curTop = currentPoint.Y;
            }

            //Теперь ищем точку, на которой процент темноты областей в левой области равен 100%. Пусть ширина матрицы равна 3 пикселя
            ushort areaWidth = 3;
            short widthIndex = 0;
            double darkPercent = 0;
            while (darkPercent < 0.99 && (curLeft - areaWidth - widthIndex) > 0)
            {
                darkPercent = CalcPercentDarkArea(frame, 
                    (ushort)(curLeft - areaWidth - widthIndex), curTop,
                    (ushort)(curLeft - widthIndex), curBottom);
                widthIndex++;
            }
            left = curLeft - areaWidth - widthIndex >= 0 ? (ushort)(curLeft - areaWidth - widthIndex) : (ushort)0;
            //Теперь ищем точку, на которой процент темноты областей в правой области равен 100%. Пусть высота матрицы равна 3 пикселя
            widthIndex = 0;
            darkPercent = 0;
            while (darkPercent < 0.99 && (curRight + areaWidth + widthIndex) < frame.Data.GetLength(1))
            {
                darkPercent = CalcPercentDarkArea(frame, (ushort)(curRight + widthIndex), curTop,
                    (ushort)(curRight + widthIndex + areaWidth), curBottom);
                widthIndex++;
            }
            right = curRight + widthIndex + areaWidth >= frame.Data.GetLength(0) ?
                (ushort)(frame.Data.GetLength(1) - 1) : (ushort)(curRight + widthIndex + areaWidth);

        }        
        void ClearFrameArea(Image<Gray, Byte> frame, ushort left, ushort right, ushort top, ushort bottom)
        {
            //Удаление точек по оси X от точки старта до следующего, максимально удалённого от исходного элемента
            for (int i = left; i < right; i++)
            {
                for (int j = top; j < bottom; j++)
                {
                    if (j < frame.Data.GetLength(0) && j > 0 && i < frame.Data.GetLength(1) && i > 0)
                    {
                        frame.Data[j, i, 0] = 0;
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="behavior">Поведение поиска следующей точки: 0 - точка слева, а область справа, снизу и сверху;</param>
        /// <param name="templateSize"></param>
        /// <param name="currentPoint"></param>
        /// <param name="grayFrame"></param>
        /// <returns></returns>
        private CatheterPoint BorderAnalysisAlgorithm_NextSkeletPoint(
            ref byte behavior, ushort templateSize, 
            CatheterPoint currentPoint, Image<Gray, Byte> grayFrame,
            ref bool hasCross)
        {
            int currentTemplateSize = templateSize;
            //Установка размров для исследования матрицы
            //Зависит от поведения
            ushort topBorder = 0;
            ushort rightBorder = 0;
            ushort bottomBorder = 0;
            ushort leftBorder = 0;
            CalcMatrixRange(currentPoint, behavior, grayFrame.Data.GetLength(0), grayFrame.Data.GetLength(1), templateSize,
                ref topBorder, ref bottomBorder, ref rightBorder, ref leftBorder);

            //Идея: смотрим объекты, лежащие на границе просмотровой матрицы, вместо определения наиболее отстранённой точки
            byte countFoundPoints = 0;
            var FoundPoints = FindBorderPoints(
                grayFrame, behavior, 
                new List<ushort>() { leftBorder, topBorder, rightBorder, bottomBorder }, ref countFoundPoints);

            if (FoundPoints == null)
            {
                var matr = StateMatrix(grayFrame, leftBorder, topBorder, rightBorder, bottomBorder);
                string strMatrix = "";
                for (ushort i = 0; i < matr.GetLength(0); i++)
                {
                    for (ushort j = 0; j < matr.GetLength(1); j++)
                    {
                        strMatrix += matr[i, j].ToString() + " ";
                    }
                    strMatrix += System.Environment.NewLine;
                }
            }

            if (FoundPoints != null)
            {
                CatheterPoint nextPoint = null;
                int indexNextPoint = -1;
                //На границе обнаружена лишь одна точка, пусть она и будет следующей
                if (countFoundPoints == 1)
                {
                    indexNextPoint = FoundPoints.FindIndex(x => x != null);
                    nextPoint = new CatheterPoint(FoundPoints[indexNextPoint]);
                    behavior = GetNextBehavior(indexNextPoint);

                    //TODO
                    //Почему то плохо обрабатывается конец катетера...!!!!!!!!!

                    //Онуление=)
                    //Очистить использованные данные
                    //ClearUsedPixels(grayFrame, behavior, nextPoint, currentPoint, topBorder, bottomBorder, rightBorder, leftBorder);
                    CarefulClear(grayFrame, currentPoint, nextPoint, leftBorder, topBorder, rightBorder, bottomBorder, behavior);

                    if (behavior != 100)
                        return nextPoint;
                    else
                        return null;
                }
                if (countFoundPoints == 2) // Скорее всего сложенный катетр, просто выберем одну из точек
                {
                    var listBorderPoints = FoundPoints.FindAll(x => x != null);
                    if (IsClosePoints(listBorderPoints[0], listBorderPoints[1], this.CleaningAreaSize))
                    {
                        //Искусственно запоним единицами близкие блоки... Не помогло
                        SyntheticFill(grayFrame, listBorderPoints[0], listBorderPoints[1]);

                        nextPoint = new CatheterPoint(listBorderPoints[(new Random()).Next(0, listBorderPoints.Count)]);
                        indexNextPoint = FoundPoints.FindIndex(x => (x != null && x.X == nextPoint.X && x.Y == nextPoint.Y && x.Z == nextPoint.Z));
                        behavior = GetNextBehavior(indexNextPoint);
                        //Очистить использованные данные
                        //ClearUsedPixels(grayFrame, behavior, nextPoint, currentPoint, topBorder, bottomBorder, rightBorder, leftBorder);
                        CarefulClear(grayFrame, currentPoint, nextPoint, leftBorder, topBorder, rightBorder, bottomBorder, behavior);
                    }
                    else
                    {
                        List<CatheterPoint> neighbours = new List<CatheterPoint>();
                        byte[,] visitMatrix = null;
                        for (int i = 0; i < listBorderPoints.Count; i++)
                        {
                            if (!IsBindingPoints(grayFrame, currentPoint, behavior, listBorderPoints[i],
                                0, leftBorder, topBorder, rightBorder, bottomBorder, ref visitMatrix))
                            {
                                neighbours.Add(new CatheterPoint(listBorderPoints[i]));
                            }
                        }
                        if (neighbours.Count == 0) //Если вдруг никто не связан кривой, то вероятно глюк или что то вроде этого, продумать обработку...
                            return null;
                        //Состояние близкого подхода другой части катетера. Надо правильно почистить данные
                        if (neighbours.Count == 1)
                        {
                            nextPoint = new CatheterPoint(neighbours[0]);
                            indexNextPoint = FoundPoints.FindIndex(x => (x != null && x.X == nextPoint.X && x.Y == nextPoint.Y && x.Z == nextPoint.Z));
                            behavior = GetNextBehavior(indexNextPoint);
                            //Аккуратно удалем просмотренные элементы, кроме следующей точки и точек, лежащих рядом с ней, по расскрашенной матрице=)
                            if (visitMatrix != null) //Запуск тщательного удаления
                            {
                                CarefulRemoval(grayFrame, visitMatrix, leftBorder, topBorder, rightBorder, bottomBorder, behavior);
                            }
                        }
                        if (neighbours.Count == 2) //Состояние петли
                        {

                        }
                    }

                    if (behavior != 100)
                        return nextPoint;
                    else
                        return null;
                }
                if (countFoundPoints == 3)
                {
                    //Запуск алгоритма локальной обработки петли
                    hasCross = true;
                }
                if (countFoundPoints == 4)
                {
                    //Такого вообще быть не может, тестирование покажет
                    return null;
                }
            }
            return null;
        }

        void SyntheticFill(Image<Gray, Byte> frame, CatheterPoint first, CatheterPoint second)
        {
            //Расчёт границ матрицы исскуственного заполнения
            ushort leftBorder = first.X < second.X ? first.X : second.X;
            ushort rightBorder = first.X < second.X ? second.X : first.X;
            ushort topBorder = first.Y < second.Y ? first.Y : second.Y;
            ushort bottomBorder = first.Y < second.Y ? second.Y : first.Y;
            for (ushort i = topBorder; i <= bottomBorder; i++)
                for (ushort j = leftBorder; j <= rightBorder; j++)
                {
                    frame.Data[i, j, 0] = 255;
                }
        }

        void CarefulRemoval(Image<Gray, Byte> frame, byte[,] visitMatrix, ushort leftBorder, ushort topBorder, ushort rightBorder, ushort bottomBorder, byte nexBehavior)
        {
            try
            {
                ushort curleftBorder = leftBorder;
                ushort curRightBorder = rightBorder;
                ushort curTopBorder = topBorder;
                ushort curBottomBorder = bottomBorder;
                //Данный свитч подскажет нам, как именно надо удалять элементы, и какую часть оставить
                switch (nexBehavior)
                {
                    case 0:
                        curRightBorder--;
                        break;
                    case 1:
                        curBottomBorder--;
                        break;
                    case 2:
                        curleftBorder++;
                        break;
                    case 3:
                        curTopBorder++;
                        break;
                }


                ushort i = 0;
                ushort j = 0;
                try
                {
                    for (i = curTopBorder; i <= curBottomBorder; i++)
                        for (j = curleftBorder; j <= curRightBorder; j++)
                        {
                            if (visitMatrix[i - topBorder, j - leftBorder] > 0)
                            {
                                frame.Data[i, j, 0] = 0;
                            }
                        }
                }
                catch (Exception ex)
                {
                    string exM = ex.Message;
                }
                //Добавить деликатное удаление одиноких пикселей
                RemoveAlonePixel(frame, (ushort)(curleftBorder + 1), (ushort)(curTopBorder + 1), (ushort)(curRightBorder - 1), (ushort)(curBottomBorder - 1));

            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
            }
        }

        /// <summary>
        /// Метод очищающий одиночные матрицы
        /// TODO
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="curleftBorder"></param>
        /// <param name="curTopBorder"></param>
        /// <param name="curRightBorder"></param>
        /// <param name="curBottomBorder"></param>
        void RemoveAlonePixel(Image<Gray, Byte> frame, ushort curleftBorder, ushort curTopBorder, ushort curRightBorder, ushort curBottomBorder)
        {
            ushort i = 0;
            ushort j = 0;
            short left = -1, right = 1, top = -1, bottom = 1;
            byte countNeighbors = 0;
            for (i = curTopBorder; i <= curBottomBorder; i++)
                for (j = curleftBorder; j <= curRightBorder; j++)
                {
                    if (frame.Data[i, j, 0] > 0)
                    {
                        for (short iInside = top; i <= bottom; i++)
                            for (short jInside = left; j <= right; j++)
                            {
                                if (!(iInside == 0 && jInside == 0) && frame.Data[i + iInside, j + jInside, 0] > 0)
                                    countNeighbors++;
                            }
                        if (countNeighbors <= 1)
                            frame.Data[i, j, 0] = 0;
                        countNeighbors = 0;
                    }
                }
        }

        /// <summary>
        /// Алгоритм, в зависимости от поведения исходной точки и возможного поведения следующей запускаем алгоритм прохода по границе катетера=)
        /// TODO: нужны тесты, что бы проверить работоспособность
        /// </summary>
        /// <param name="grayFrame"></param>
        /// <param name="currentPoint"></param>
        /// <param name="possibleNextPoint"></param>
        bool IsBindingPoints(Image<Gray, Byte> grayFrame, CatheterPoint currentPoint, byte curBeh, CatheterPoint possibleNextPoint, byte nextBeh,
            ushort left, ushort top, ushort right, ushort bottom, ref byte[,] visitMatrix)
        {
            var matr = StateMatrix(grayFrame, left, top, right, bottom);
            string strMatrix = "";
            for (ushort i = 0; i < matr.GetLength(0); i++)
            {
                for (ushort j = 0; j < matr.GetLength(1); j++)
                {
                    strMatrix += matr[i, j].ToString() + " ";
                }
                strMatrix += System.Environment.NewLine;
            }

            byte[,] colorMatrix = new byte[matr.GetLength(0), matr.GetLength(1)];


            bool isPointBind = DFS(colorMatrix, matr, new Tuple<ushort, ushort>((ushort)(currentPoint.Y - top), (ushort)(currentPoint.X - left)), 
                new Tuple<ushort, ushort>((ushort)(possibleNextPoint.Y - top), (ushort)(possibleNextPoint.X - left)),
                 (ushort)(matr.GetLength(0)), (ushort)(matr.GetLength(1))); // Явные проблемы с удалением данных

            string strColorMatrix = "";
            for (ushort i = 0; i < colorMatrix.GetLength(0); i++)
            {
                for (ushort j = 0; j < colorMatrix.GetLength(1); j++)
                {
                    strColorMatrix += colorMatrix[i, j].ToString() + " ";
                }
                strColorMatrix += System.Environment.NewLine;
            }

            visitMatrix = colorMatrix;

            return isPointBind;
        }

        /// <summary>
        /// Не рекурсивный алгоритм обхода графа(подматрицы фрейма), основанный на стеке
        /// </summary>
        /// <param name="colorMatrix">Матрица, показывающая где конкретно были найдены пиксели</param>
        /// <param name="realMatrix">Подматрица кадра</param>
        /// <param name="startPoint">Стартовая точка запуска алгоритма обхода</param>
        /// <param name="goalPoint">Целевая точка обхода. Значение NULL приводит к обходу всего графа</param>
        /// <param name="row">Количество строк матрицы</param>
        /// <param name="col">Количество столбцов матрицы</param>
        /// <returns>True, если целевая точка не null и она была найдена в матрице, иначе False</returns>
        bool DFSonStack(byte[,] colorMatrix, byte[,] realMatrix, Tuple<ushort, ushort> startPoint, Tuple<ushort, ushort> goalPoint, ushort row, ushort col)
        {
            bool isGoalFind = false;
            Stack<Tuple<ushort, ushort>> s = new Stack<Tuple<ushort, ushort>>();
            s.Push(startPoint);
            while (!(s.Count == 0))
            {
                Tuple<ushort, ushort> v = s.Pop();

                for (int i = -1; i <= 1; i++)
                {
                    for (int j = -1; j <= 1; j++)
                    {
                        int indexI = v.Item1 + i;
                        int indexJ = v.Item2 + j;
                        if (indexI >= 0 && indexI < row && indexJ < col && indexJ >= 0
                            && (i != 0 || j != 0)) //Проверка границ
                        {
                            if (realMatrix[v.Item1 + i, v.Item2 + j] > 0 && colorMatrix[v.Item1 + i, v.Item2 + j] == 0)
                            {
                                colorMatrix[v.Item1 + i, v.Item2 + j] = 1;
                                s.Push(new Tuple<ushort, ushort>((ushort)(v.Item1 + i), (ushort)(v.Item2 + j)));
                                if (goalPoint != null && goalPoint.Item1 == v.Item1 + i && goalPoint.Item2 == v.Item2 + j)
                                {
                                    colorMatrix[v.Item1 + i, v.Item2 + j] = 10;
                                    isGoalFind = true;
                                }
                            }
                        }
                    }
                }
            }
            return isGoalFind;
        }

        void CarefulClear(Image<Gray, Byte> frame, 
            CatheterPoint currentPoint, CatheterPoint nextPoint, 
            ushort left, ushort top, ushort right, ushort bottom, byte nextBehavior)
        {
            //Получаем текущую часть фрейма
            var currentFramePart = StateMatrix(frame, left, top, right, bottom);
            #if DEBUG
            string strMatrix1 = "";
            for (ushort i = 0; i < currentFramePart.GetLength(0); i++)
            {
                for (ushort j = 0; j < currentFramePart.GetLength(1); j++)
                {
                    strMatrix1 += currentFramePart[i, j].ToString() + " ";
                }
                strMatrix1 += System.Environment.NewLine;
            }
            #endif



            byte[,] visitedMatrix = new byte[currentFramePart.GetLength(0), currentFramePart.GetLength(1)];
            if (nextPoint != null)
            {
                bool isPointsBind = DFSonStack(visitedMatrix, currentFramePart,
                    new Tuple<ushort, ushort>((ushort)(currentPoint.Y - top), (ushort)(currentPoint.X - left)),
                    new Tuple<ushort, ushort>((ushort)(nextPoint.Y - top), (ushort)(nextPoint.X - left)),
                    (ushort)(currentFramePart.GetLength(0)), (ushort)(currentFramePart.GetLength(1)));
                if (!isPointsBind)
                {
                    DFSonStack(visitedMatrix, currentFramePart,
                    new Tuple<ushort, ushort>((ushort)(nextPoint.Y - top), (ushort)(nextPoint.X - left)),
                    null,
                    (ushort)(currentFramePart.GetLength(0)), (ushort)(currentFramePart.GetLength(1)));
                }
            }
            else
            {
               DFSonStack(visitedMatrix, currentFramePart,
                    new Tuple<ushort, ushort>((ushort)(currentPoint.Y - top), (ushort)(currentPoint.X - left)),
                    null,
                    (ushort)(currentFramePart.GetLength(0)), (ushort)(currentFramePart.GetLength(1)));
            }

            if (left > 500)
            {
                currentFramePart = StateMatrix(frame, left, top, right, bottom);
#if DEBUG
                string strMatrix3 = "";
                for (ushort i = 0; i < currentFramePart.GetLength(0); i++)
                {
                    for (ushort j = 0; j < currentFramePart.GetLength(1); j++)
                    {
                        strMatrix3 += currentFramePart[i, j].ToString() + " ";
                    }
                    strMatrix3 += System.Environment.NewLine;
                }
                int s1 = 0;
#endif
            }

            CarefulRemoval(frame, visitedMatrix, left, top, right, bottom, nextBehavior);

        }

        bool DFS(byte[,] colorMatrix, byte[,] realMatrix, Tuple<ushort, ushort> startPoint, Tuple<ushort, ushort> goalPoint, ushort row, ushort col)
        {
            bool resultFinding = false;
            colorMatrix[startPoint.Item1, startPoint.Item2] = 1;
            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    int indexI = startPoint.Item1 + i;
                    int indexJ = startPoint.Item2 + j;
                    if (indexI >= 0 && indexI < row && indexJ < col && indexJ >= 0
                        && (i != 0 || j != 0)) //Проверка границ
                    {
                        if (realMatrix[startPoint.Item1 + i, startPoint.Item2 + j] > 0 && colorMatrix[startPoint.Item1 + i, startPoint.Item2 + j] == 0)
                        {
                            colorMatrix[startPoint.Item1 + i, startPoint.Item2 + j] = 1;
                            if (goalPoint.Item1 == startPoint.Item1 + i && goalPoint.Item2 == startPoint.Item2 + j)
                            {
                                colorMatrix[startPoint.Item1 + i, startPoint.Item2 + j] = 10;
                                resultFinding = true;
                                i = 2; j = 2;
                            }
                            else
                                resultFinding = DFS(colorMatrix, realMatrix, new Tuple<ushort, ushort>((ushort)(startPoint.Item1 + i), (ushort)(startPoint.Item2 + j)), goalPoint, row, col);
                        }
                    }
                }
            }
            colorMatrix[startPoint.Item1, startPoint.Item2] = 2;
            return resultFinding;
        }

        /// <summary>
        /// Поиск левой границы катетера для данной точки
        /// </summary>
        /// <param name="grayFrame">Фрейм для поиска границ</param>
        /// <param name="currentPoint">Точка, для которой ищется граница</param>
        /// <returns>Граничная точка</returns>
        CatheterPoint FindLeftBorderPoint(Image<Gray, Byte> grayFrame, CatheterPoint currentPoint,
            ushort left)
        {
            int leftBorder = 0;
            while (left < (currentPoint.X - leftBorder - 1)
                && grayFrame.Data[currentPoint.Y, currentPoint.X - leftBorder - 1, 0] > 0)
            {
                leftBorder++;
            }
            return new CatheterPoint((ushort)(currentPoint.X - leftBorder), currentPoint.Y, 0);
        }

        /// <summary>
        /// Поиск правой границы катетера для данной точки
        /// </summary>
        /// <param name="grayFrame">Фрейм для поиска границ</param>
        /// <param name="currentPoint">Точка, для которой ищется граница</param>
        /// <returns>Граничная точка</returns>
        CatheterPoint FindRightBorderPoint(Image<Gray, Byte> grayFrame, CatheterPoint currentPoint,
            ushort right)
        {
            int rightBorder = 0;
            while (right > (currentPoint.X + rightBorder + 1)
                && grayFrame.Data[currentPoint.Y, currentPoint.X + rightBorder + 1, 0] > 0)
            {
                rightBorder++;
            }
            return new CatheterPoint((ushort)(currentPoint.X + rightBorder), currentPoint.Y, 0);
        }

        /// <summary>
        /// Поиск нижней границы катетера для данной точки
        /// </summary>
        /// <param name="grayFrame">Фрейм для поиска границ</param>
        /// <param name="currentPoint">Точка, для которой ищется граница</param>
        /// <returns>Граничная точка</returns>
        CatheterPoint FindDownBorderPoint(Image<Gray, Byte> grayFrame, CatheterPoint currentPoint,
            ushort bottom)
        {
            int downBorder = 0;
            while (bottom > (currentPoint.Y + downBorder + 1) 
                && grayFrame.Data[currentPoint.Y + downBorder + 1, currentPoint.X, 0] > 0)
            {
                downBorder++;
            }
            return new CatheterPoint(currentPoint.X, (ushort)(currentPoint.Y + downBorder), 0);
        }
        /// <summary>
        /// Поиск верхней границы катетера для данной точки
        /// </summary>
        /// <param name="grayFrame">Фрейм для поиска границ</param>
        /// <param name="currentPoint">Точка, для которой ищется граница</param>
        /// <returns>Граничная точка</returns>
        CatheterPoint FindUpBorderPoint(Image<Gray, Byte> grayFrame, CatheterPoint currentPoint,
            ushort top)
        {
            int upBorder = 0;
            while (top < (currentPoint.Y - upBorder - 1)
                && grayFrame.Data[currentPoint.Y - upBorder - 1, currentPoint.X, 0] > 0)
            {
                upBorder--;
            }
            return new CatheterPoint(currentPoint.X, (ushort)(currentPoint.Y - upBorder), 0);
        }

        /// <summary>
        /// Получит ширину катетера по вертикале
        /// </summary>
        /// <param name="frame">Область, в которой определяем ширину катетера</param>
        /// <param name="currentPoint"></param>
        /// <returns></returns>
        List<CatheterPoint> GetHeightCurrentPoint(Image<Gray, Byte> grayFrame, CatheterPoint currentPoint, ushort topBorder, ushort bottomBorder)
        {
            if (currentPoint == null || grayFrame == null) return null;
            int upHeight = 1;
            while (0 <= (currentPoint.Y - upHeight) && grayFrame.Data[currentPoint.Y - upHeight, currentPoint.X, 0] > 0)
            {
                upHeight++;
            }
            int downHeight = 1;
            while (grayFrame.Data.GetLength(0) > (currentPoint.Y + downHeight) && grayFrame.Data[currentPoint.Y + downHeight, currentPoint.X, 0] > 0)
            {
                downHeight++;
            }
            //[up, mid, down]
            List<CatheterPoint> verticalPoints = new List<CatheterPoint>() {
                new CatheterPoint(currentPoint.X, (ushort)(currentPoint.Y - upHeight), 0), 
                new CatheterPoint(currentPoint),
                new CatheterPoint(currentPoint.X, (ushort)(currentPoint.Y + downHeight), 0)
            };
            return verticalPoints;
        }
        /*
        List<CatheterPoint> GetWidthCurrentPoint(ushort[,] pieceFrame, CatheterPoint currentPoint)
        {
            if (currentPoint == null || pieceFrame == null) return null;
            int leftWidth = 1;
            while (0 <= (currentPoint.Y - leftWidth) && pieceFrame[currentPoint.Y - leftWidth, currentPoint.X] > 0)
            {
                upHeight++;
            }
            int rightWidth = 1;
            while (pieceFrame.GetLength(0) > (currentPoint.Y + downHeight) && pieceFrame[currentPoint.Y + downHeight, currentPoint.X] > 0)
            {
                downHeight++;
            }
            //[up, mid, dows]
            List<CatheterPoint> verticalPoints = new List<CatheterPoint>() {
                new CatheterPoint(currentPoint.X, (ushort)(currentPoint.Y - upHeight), 0),
                new CatheterPoint(currentPoint),
                new CatheterPoint(currentPoint.X, (ushort)(currentPoint.Y + downHeight), 0)
            };
            return verticalPoints;
        }
        */


        CatheterPoint PointsDensityEstimation(List<CatheterPoint> points, CatheterPoint ctartPoint, byte curBehavior, ushort[,] matrix)
        {
            int matrixrow = matrix.GetLength(0);
            int matrixcol = matrix.GetLength(1);


            

            return null;
        }

        //Список [left, top, right, bottom]
        List<double> Behaiver0(ushort[,] matrix)
        {
            //[left, top, right, bottom]
            List<double> resList = new List<double>() { 0, 0, 0, 0 };
            int matrixrow = matrix.GetLength(0);
            int matrixcol = matrix.GetLength(1);

            int X1 = 0, Y1 = matrixrow / 2 - 1, X2 = matrixcol - 1, Y2 = matrixrow - 2;
            int XU2 = matrixcol - 1, YU2 = 1;

            int counter = 0;
            int importantCounter = 0;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsOverLine(X1, Y1, XU2, YU2, j, i))//IsUnderLine(X1, Y1, X2, Y2, j, i))
                    {
                        counter++;
                        if (matrix[i, j] > 0)
                            importantCounter++;
                    }
                }
            }
            resList[1] = (double)importantCounter / counter;

            counter = 0;
            importantCounter = 0;
            Y1 = matrixrow / 2;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsUnderLine(X1, Y1, X2, Y2, j, i))
                    {
                        counter++;
                        if (matrix[i, j] > 0)
                            importantCounter++;
                    }
                }
            }
            resList[3] = (double)importantCounter / counter;

            counter = 0;
            importantCounter = 0;
            int Y11 = matrixrow / 2 - 1, Y22 = matrixrow / 2;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (!IsOverLine(X1, Y11, XU2, YU2, j, i) && !IsUnderLine(X1, Y22, X2, Y2, j, i))//IsUnderLine(X1, Y1, X2, Y2, j, i))
                    {
                        counter++;
                        if (matrix[i, j] > 0)
                            importantCounter++;
                    }
                }
            }
            resList[2] = (double)importantCounter / counter;

            return resList;
        }
        void Behaiver1(ushort[,] matrix)
        {
            //[left, top, right, bottom]
            List<double> resList = new List<double>() { 0, 0, 0, 0 };

            int matrixrow = matrix.GetLength(0);
            int matrixcol = matrix.GetLength(1);

            int X1 = matrixcol / 2, Y1 = 0, X2 = 1, Y2 = matrixrow - 1;
            int XU2 = matrixcol - 2, YU2 = matrixrow - 1;

            int counter = 0;
            int importantCounter = 0;

            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsOverLine(X1, Y1, XU2, YU2, j, i))
                    {
                        counter++;
                        if (matrix[i, j] > 0)
                            importantCounter++;
                    }
                }
            }
            resList[2] = (double)importantCounter / counter;


            counter = 0;
            importantCounter = 0;
            X1 = matrixcol / 2 - 1;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsOverLine(X1, Y1, X2, Y2, j, i))
                    {
                        counter++;
                        if (matrix[i, j] > 0)
                            importantCounter++;
                    }
                }
            }
            resList[0] = (double)importantCounter / counter;

            int X11 = matrixcol / 2 - 1;
            X1 = matrixcol / 2;

            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (!IsOverLine(X11, Y1, X2, Y2, j, i) && !IsOverLine(X1, Y1, XU2, YU2, j, i))
                    {
                        matrix[i, j] = 3;
                    }
                }
            }
        }
        /*
            static int matrixrow = 14;
            static int matrixcol = 7;
        */
        void Behaiver2(ushort[,] matrix)
        {
            //[left, top, right, bottom]
            List<double> resList = new List<double>() { 0, 0, 0, 0 };

            int matrixrow = matrix.GetLength(0);
            int matrixcol = matrix.GetLength(1);

            int X1 = matrixcol, Y1 = matrixrow / 2, X2 = 0, Y2 = matrixrow - 1;
            int XU2 = 0, YU2 = 0;

            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsOverLine(X1, Y1, XU2, YU2, j, i))
                    {
                        matrix[i, j] = 1;
                    }
                }
            }


            Y1 = matrixrow / 2 - 1;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsUnderLine(X1, Y1, X2, Y2, j, i))
                    {
                        matrix[i, j] = 2;
                    }
                }
            }


            Y1 = matrixrow / 2;
            int Y11 = matrixrow / 2 - 1;

            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (!IsUnderLine(X1, Y11, X2, Y2, j, i) && !IsOverLine(X1, Y1, XU2, YU2, j, i))
                    {
                        matrix[i, j] = 3;
                    }
                }
            }
        }
        /*
            static int matrixrow = 7;
            static int matrixcol = 14;
        */
        void Behaiver3(ushort[,] matrix)
        {
            //[left, top, right, bottom]
            List<double> resList = new List<double>() { 0, 0, 0, 0 };

            int matrixrow = matrix.GetLength(0);
            int matrixcol = matrix.GetLength(1);

            int X1 = matrixcol / 2, Y1 = matrixrow, X2 = matrixcol, Y2 = 0;
            int XU2 = 0, YU2 = 0;

            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsUnderLine(X1, Y1, XU2, YU2, j, i))
                    {
                        matrix[i, j] = 1;
                    }
                }
            }
            X1 = matrixcol / 2 - 1;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (IsUnderLine(X1, Y1, X2, Y2, j, i))
                    {
                        matrix[i, j] = 2;
                    }
                }
            }
            int X11 = matrixcol / 2;
            for (int i = 0; i < matrixrow; i++)
            {
                for (int j = 0; j < matrixcol; j++)
                {
                    if (!IsUnderLine(X1, Y1, X2, Y2, j, i) && !IsUnderLine(X11, Y1, XU2, YU2, j, i))
                    {
                        matrix[i, j] = 3;
                    }
                }
            }
        }

        static bool IsUnderLine(int x1, int y1, int x2, int y2, int x, int y)
        {
            int localY = (int)(y1 + ((double)(x - x1) / (x2 - x1)) * (y2 - y1));
            if (y >= localY)
                return true;
            return false;
        }

        static bool IsOverLine(int x1, int y1, int x2, int y2, int x, int y)
        {
            int localY = (int)(y1 + ((double)(x - x1) / (x2 - x1)) * (y2 - y1));
            if (y <= localY)
                return true;
            return false;
        }


        bool IsClosePoints(CatheterPoint firstPoint, CatheterPoint secondPoint, ushort size)
        {
            ushort curLen = (ushort)(Math.Sqrt(Math.Pow(firstPoint.X - secondPoint.X, 2)
                + Math.Pow(firstPoint.Y - secondPoint.Y, 2)
                + Math.Pow(firstPoint.Z - secondPoint.Z, 2)));
            if (curLen > (size + 1)) //Возможно +1 лишний
                return false;
            return true;
        }

        byte[,] StateMatrix(Image<Gray, Byte> frame, ushort leftBorder, ushort topBorder, ushort rightBorder, ushort bottomBorder){
            byte[,] resMatrix = new byte[bottomBorder - topBorder + 1, rightBorder - leftBorder + 1];
            try
            {
                for (ushort i = topBorder; i <= bottomBorder; i++)
                    for (ushort j = leftBorder; j <= rightBorder; j++)
                        resMatrix[i - topBorder, j - leftBorder] = frame.Data[i, j, 0] > 0 ? (byte)1 : (byte)0;
            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
            }

            return resMatrix;
        }

        byte[,] TestMatrix(Image<Gray, Byte> frame, CatheterPoint currentPoint, CatheterPoint possibleNextPoint, ushort leftBorder, ushort topBorder, ushort rightBorder, ushort bottomBorder)
        {
            var first = new Tuple<ushort, ushort>((ushort)(currentPoint.Y - topBorder), (ushort)(currentPoint.X - leftBorder));
            var second = new Tuple<ushort, ushort>((ushort)(possibleNextPoint.Y - topBorder), (ushort)(possibleNextPoint.X - leftBorder));
            byte[,] resMatrix = new byte[bottomBorder - topBorder + 1, rightBorder - leftBorder + 1];
            try
            {
                for (ushort i = topBorder; i <= bottomBorder; i++)
                    for (ushort j = leftBorder; j <= rightBorder; j++)
                    {
                        resMatrix[i - topBorder, j - leftBorder] = frame.Data[i, j, 0] > 0 ? (byte)1 : (byte)0;
                    }
                        
            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
            }
            return resMatrix;
        }



        byte GetNextBehavior(int indexNextPoint)
        {
            switch (indexNextPoint)
            {
                case 0:
                    return 2;
                case 1:
                    return 3;
                case 2:
                    return 0;
                case 3:
                    return 1;
                default:
                    return 100;//Ошибка!!!
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="behavior">Поведение поиска следующей точки: 0 - точка слева, а область справа, снизу и сверху;</param>
        /// <param name="templateSize"></param>
        /// <param name="currentPoint"></param>
        /// <param name="grayFrame"></param>
        /// <returns></returns>
        private CatheterPoint MatrixSeparation_NextSkeletPoint(ref byte behavior, ushort templateSize, CatheterPoint currentPoint, Image<Gray, Byte> grayFrame)
        {
            int currentTemplateSize = templateSize;
            List<CatheterPoint> vectorizePoints = new List<CatheterPoint>();
            //Установка размров для исследования матрицы
            //Зависит от поведения
            ushort topBorder = 0;
            ushort rightBorder = 0;
            ushort bottomBorder = 0;
            ushort leftBorder = 0;
            CalcMatrixRange(currentPoint, behavior, grayFrame.Data.GetLength(0), grayFrame.Data.GetLength(1), templateSize,
                ref topBorder, ref bottomBorder, ref rightBorder, ref leftBorder);

            //Добавляем ненулевые точки в список
            for (ushort i = topBorder; i < bottomBorder; i++)
            {
                for (ushort j = leftBorder; j < rightBorder; j++)
                {
                    if (grayFrame.Data[i, j, 0] > 0)
                    {
                        vectorizePoints.Add(new CatheterPoint(j, i, 0));
                    }
                }
            }

            //В ходе работы метода создаётся новая точка
            CatheterPoint nextPoint = GetNextSkeletPoint(currentPoint, vectorizePoints);
            //Очищаем память
            vectorizePoints.Clear(); vectorizePoints = null;
            if (nextPoint != null)
            {
                //Определяем следующий тип поведения
                behavior = GetNewBehavior(behavior, currentPoint, nextPoint, topBorder, bottomBorder, rightBorder, leftBorder);
                //Очистить использованные данные
                ClearUsedPixels(grayFrame, behavior, nextPoint, currentPoint, topBorder, bottomBorder, rightBorder, leftBorder);
            }
            return nextPoint;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="behavior"></param>
        /// <param name="borderValues">[left, top, right, bottom]</param>
        /// <returns></returns>
        List<CatheterPoint> FindBorderPoints(Image<Gray, Byte> frame, byte behavior, List<ushort> borderValues, ref byte countFoundPoints)
        {
            //  Результирующий вектор пусть состоит из 4 точек, характеризующие состояние границ на матрице
            //  [left, top, right, bottom]
            List<CatheterPoint> resultsList = new List<CatheterPoint>() { null, null, null, null };
            bool isPointsFind = false;
            countFoundPoints = 0;
            //Let's case
            switch (behavior)
            {
                case 0:
                    //Поиск по границе top
                    for (ushort i = borderValues[0]; i < borderValues[2]; i++)
                        if (frame.Data[borderValues[1], i, 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[1] = new CatheterPoint(i, borderValues[1], 0);
                        }           
                    //Поиск по границе right
                    for (ushort i = borderValues[1]; i < borderValues[3]; i++)
                        if (frame.Data[i, borderValues[2], 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[2] = new CatheterPoint(borderValues[2], i, 0);
                        }
                    //Поиск по границе bottom
                    for (ushort i = borderValues[0]; i < borderValues[2]; i++)
                        if (frame.Data[borderValues[3], i, 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[3] = new CatheterPoint(i, borderValues[3], 0);
                        }
                    break;
                case 1:
                    //Поиск по границе left
                    for (ushort i = borderValues[1]; i < borderValues[3]; i++)
                        if (frame.Data[i, borderValues[0], 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[0] = new CatheterPoint(borderValues[0], i, 0);
                        }
                    //Поиск по границе right
                    for (ushort i = borderValues[1]; i < borderValues[3]; i++)
                        if (frame.Data[i, borderValues[2], 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[2] = new CatheterPoint(borderValues[2], i, 0);
                        }
                    //Поиск по границе bottom
                    for (ushort i = borderValues[0]; i < borderValues[2]; i++)
                        if (frame.Data[borderValues[3], i, 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[3] = new CatheterPoint(i, borderValues[3], 0);
                        }
                    break;
                case 2:
                    //Поиск по границе left
                    for (ushort i = borderValues[1]; i < borderValues[3]; i++)
                        if (frame.Data[i, borderValues[0], 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[0] = new CatheterPoint(borderValues[0], i, 0);
                        }
                    //Поиск по границе top
                    for (ushort i = borderValues[0]; i < borderValues[2]; i++)
                        if (frame.Data[borderValues[1], i, 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[1] = new CatheterPoint(i, borderValues[1], 0);
                            break;
                        }
                    //Поиск по границе bottom
                    for (ushort i = borderValues[0]; i < borderValues[2]; i++)
                        if (frame.Data[borderValues[3], i, 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[3] = new CatheterPoint(i, borderValues[3], 0);
                            break;
                        }
                    break;
                case 3:
                    //Поиск по границе left
                    for (ushort i = borderValues[1]; i < borderValues[3]; i++)
                        if (frame.Data[i, borderValues[0], 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[0] = new CatheterPoint(borderValues[0], i, 0);
                        }
                    //Поиск по границе right
                    for (ushort i = borderValues[1]; i < borderValues[3]; i++)
                        if (frame.Data[i, borderValues[2], 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[2] = new CatheterPoint(borderValues[2], i, 0);
                        }
                    //Поиск по границе top
                    for (ushort i = borderValues[0]; i < borderValues[2]; i++)
                        if (frame.Data[borderValues[1], i, 0] > 0)
                        {
                            isPointsFind = true; //Возможно нужен break но я не уверен
                            resultsList[1] = new CatheterPoint(i, borderValues[1], 0);
                        }
                    break;
                default:
                    break;
            }

            //Если точек найдено не было - значит их и нет. Закан
            if (!isPointsFind)
            {
                /*
                var matr = StateMatrix(frame, borderValues[0], borderValues[1], borderValues[2], borderValues[3]);
                string strMatrix = "";
                for (ushort i = 0; i < matr.GetLength(0); i++)
                {
                    for (ushort j = 0; j < matr.GetLength(1); j++)
                    {
                        strMatrix += matr[i, j].ToString() + " ";
                    }
                    strMatrix += System.Environment.NewLine;
                }
                */
                return null;
            }
            countFoundPoints = 0;
            for (int i = 0; i < resultsList.Count; i++)
                if (resultsList[i] != null) countFoundPoints++;
            return resultsList;
        }


        /// <summary>
        /// Поиск следующей точки по максимальному отдалению от текущей
        /// </summary>
        /// <param name="curPoint">Текущая точка</param>
        /// <param name="points">Отобранные точки</param>
        /// <returns>Следующая точка</returns>
        private CatheterPoint GetNextSkeletPoint(CatheterPoint curPoint, List<CatheterPoint> points)
        {
            if (points != null && points.Count > 0)
            {
                double maximumDistance = Math.Sqrt(Math.Pow(curPoint.X - points[0].X, 2)
                                                    + Math.Pow(curPoint.Y - points[0].Y, 2)
                                                    + Math.Pow(curPoint.Z - points[0].Z, 2));
                CatheterPoint point = new CatheterPoint(points[0]);
                for (int i = 1; i < points.Count; i++)
                {
                    double curDistance = Math.Sqrt(Math.Pow(curPoint.X - points[i].X, 2)
                                                    + Math.Pow(curPoint.Y - points[i].Y, 2)
                                                    + Math.Pow(curPoint.Z - points[i].Z, 2));
                    if (curDistance > maximumDistance)
                    {
                        maximumDistance = curDistance;
                        point = new CatheterPoint(points[i]);
                    }
                }
                return point;
            }
            return null;
        }
    }
}
